<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trabajo_Avance_Model extends Model
{

    protected $table = 'dbo.TrabAvan';
    public static $tabla = 'dbo.TrabAvan';
    protected $primaryKey = 'FolioCaptura';

    public static $id = 'FolioCaptura';
    public static $trabajo = 'Trabajo';
    public static $avance = 'Avance';
    public static $fechaCaptura = 'FechaCaptura';
    public static $comentarios = 'Comentarios';

}
