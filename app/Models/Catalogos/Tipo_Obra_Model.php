<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tipo_Obra_Model extends Model
{
    protected $table = 'TiposObra';
    public static $tabla = 'TiposObra';
    protected $primaryKey = 'TipoObra';

    public static $id = 'TipoObra';
    public static $descripcion = 'Descripcion';


}