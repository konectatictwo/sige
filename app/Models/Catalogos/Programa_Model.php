<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Programa_Model extends Model
{
    protected $table = 'ProgCata';
    public static $tabla = 'ProgCata';
    protected $primaryKey = 'Programa';

    public static $id = 'Programa';
    public static $nombre = 'Nombre';


}