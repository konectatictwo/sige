<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Municipios_Model extends Model
{
    protected $table = 'MuniCata';
    public static $tabla = 'MuniCata';
    protected $primaryKey = 'Municipio';

    public static $id = 'Municipio';
    public static $nombre = 'Nombre';
    public static $idEstado = 'IdEstado';


}