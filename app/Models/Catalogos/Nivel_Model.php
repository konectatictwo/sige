<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Nivel_Model extends Model
{
    protected $table = 'NiveGrpo';
    public static $tabla = 'NiveGrpo';
    protected $primaryKey = 'Grupo';

    public static $id = 'Grupo';
    public static $nombre = 'Nombre';


}