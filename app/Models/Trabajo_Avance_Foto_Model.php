<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trabajo_Avance_Foto_Model extends Model
{

    protected $table = 'dbo.TrabAvanFoto';
    public static $tabla = 'dbo.TrabAvanFoto';

    public static $folioCaptura = 'FolioCaptura';
    public static $foto = 'Foto';
    public static $ubicacion = 'Ubicacion';

}
