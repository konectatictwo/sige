<?php

namespace App\Http\Controllers;

use App\Models\Catalogos\Municipios_Model;
use App\Models\Catalogos\Nivel_Model;
use App\Models\Trabajo_Avance_Foto_Model;
use App\Models\Trabajo_Avance_Model;
use App\Models\Trabajo_Completo_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;



class Mapa_Controller extends Controller
{
    const PUNTO = ".";


    /**
     * Retorna la vista del index del mapa principal
     * @author KonectaTIC
     * @return view #welcome
     */
    public function index()
    {
        $auxiliar = ["Seleccionar municipio" => "Todos"];

        $auxiliarMunicipios = Municipios_Model::select(
                                Municipios_Model::$nombre . " as nombre_mostrado",
                                Municipios_Model::$nombre
                            )->where(Municipios_Model::$idEstado, "=", '2')
                            ->orderBy(Municipios_Model::$nombre)
                            ->pluck("nombre_mostrado", Municipios_Model::$nombre)->toArray();

        $municipios = $auxiliar + $auxiliarMunicipios;

        $auxiliar = ["Seleccionar nivel" => "Todos"];
        $auxiliarNivel = Nivel_Model::orderBy(Nivel_Model::$nombre, "ASC")
        ->pluck(Nivel_Model::$nombre, DB::raw(Nivel_Model::$nombre . " as nivel"))->toArray();
        $nivel = $auxiliar + $auxiliarNivel;
        return view("welcome", compact("municipios", "nivel"));

    }

    /**
     * Retorna un JSON con la información de las obras
     * @author KonectaTIC
     * @return object JSON #Información de la obra
     */
    public function obtenerObras()
    {

        $arrayObras = Trabajo_Completo_Model::select(Trabajo_Completo_Model::$id,
            DB::raw("CONCAT(latitud ,' ', longitud) as coordenadas"),
            Trabajo_Completo_Model::$descripcion,
            Trabajo_Completo_Model::$cct,
            Trabajo_Completo_Model::$tipo,
            Trabajo_Completo_Model::$nTipo,
            Trabajo_Completo_Model::$escuela,
            Trabajo_Completo_Model::$avance,
            Trabajo_Completo_Model::$domicilio,
            Trabajo_Completo_Model::$localidad,
            Trabajo_Completo_Model::$municipio,
            Trabajo_Completo_Model::$region,
            Trabajo_Completo_Model::$nivel,
            Trabajo_Completo_Model::$ejercicio,
            Trabajo_Completo_Model::$latitud,
            Trabajo_Completo_Model::$longitud,
            Trabajo_Completo_Model::$fecInicio,
            Trabajo_Completo_Model::$fecTermino,
            Trabajo_Completo_Model::$cPrograma,
            Trabajo_Completo_Model::$prioridad,
            Trabajo_Completo_Model::$alumnosAtendidos,
            Trabajo_Completo_Model::$estatus,
            Trabajo_Completo_Model::$obra,
            Trabajo_Completo_Model::$modalidadConc,
            Trabajo_Completo_Model::$importeFallado,
            Trabajo_Completo_Model::$nSupervisor,
            Trabajo_Completo_Model::$contratista,
            Trabajo_Completo_Model::$nContratista,
            Trabajo_Completo_Model::$msgEstatus,
            Trabajo_Completo_Model::$avanceFinanciero,
            Trabajo_Completo_Model::$tipoAdj,
            Trabajo_Completo_Model::$duracionObra,
            Trabajo_Completo_Model::$escuelasAlCien,
            Trabajo_Completo_Model::$fInicioReal,
            Trabajo_Completo_Model::$fFinReal,
            Trabajo_Completo_Model::$importeContrato,
            Trabajo_Completo_Model::$contrato,
            Trabajo_Completo_Model::$nContratista,
            Trabajo_Completo_Model::$estatal,
            Trabajo_Completo_Model::$federal,
            Trabajo_Completo_Model::$municipal,
            Trabajo_Completo_Model::$otros,
            Trabajo_Completo_Model::$concurso,
            Trabajo_Completo_Model::$ejerObra,
            Trabajo_Completo_Model::$gpoNivel)
            ->where(Trabajo_Completo_Model::$longitud, "!=", '0.00000000')
            ->where(Trabajo_Completo_Model::$latitud, "!=", '0.00000000')
            ->where(Trabajo_Completo_Model::$importeFallado , ">" , 0)
            ->whereRaw(Trabajo_Completo_Model::$id . "=" . Trabajo_Completo_Model::$obra)
            ->where(Trabajo_Completo_Model::$ejercicio , ">=" , "2013")
            ->where(Trabajo_Completo_Model::$avance , ">" , 0)
            ->where(Trabajo_Completo_Model::$estatus , "!=" , "1")
            ->where(Trabajo_Completo_Model::$tipo , "!=" , "4")
            ->get()->toArray();

        return $arrayObras;

    }

    /**
     * Retorna un JSON con la información de las obras
     * @author KonectaTIC
     * @return object JSON #Información de la obra
     */
    public static function obtenerFichaObra($id)
    {

        $arrayObras = Trabajo_Completo_Model::select(Trabajo_Completo_Model::$id,
            DB::raw("CONCAT(latitud ,' ', longitud) as coordenadas"),
            Trabajo_Completo_Model::$descripcion,
            Trabajo_Completo_Model::$cct,
            Trabajo_Completo_Model::$tipo,
            Trabajo_Completo_Model::$nTipo,
            Trabajo_Completo_Model::$escuela,
            Trabajo_Completo_Model::$avance,
            Trabajo_Completo_Model::$domicilio,
            Trabajo_Completo_Model::$localidad,
            Trabajo_Completo_Model::$municipio,
            Trabajo_Completo_Model::$region,
            Trabajo_Completo_Model::$nivel,
            Trabajo_Completo_Model::$ejercicio,
            Trabajo_Completo_Model::$latitud,
            Trabajo_Completo_Model::$longitud,
            Trabajo_Completo_Model::$fecInicio,
            Trabajo_Completo_Model::$fecTermino,
            Trabajo_Completo_Model::$cPrograma,
            Trabajo_Completo_Model::$prioridad,
            Trabajo_Completo_Model::$alumnosAtendidos,
            Trabajo_Completo_Model::$estatus,
            Trabajo_Completo_Model::$obra,
            Trabajo_Completo_Model::$modalidadConc,
            Trabajo_Completo_Model::$importeFallado,
            Trabajo_Completo_Model::$nSupervisor,
            Trabajo_Completo_Model::$contratista,
            Trabajo_Completo_Model::$nContratista,
            Trabajo_Completo_Model::$msgEstatus,
            Trabajo_Completo_Model::$avanceFinanciero,
            Trabajo_Completo_Model::$tipoAdj,
            Trabajo_Completo_Model::$duracionObra,
            Trabajo_Completo_Model::$escuelasAlCien,
            Trabajo_Completo_Model::$fInicioReal,
            Trabajo_Completo_Model::$fFinReal,
            Trabajo_Completo_Model::$importeContrato,
            Trabajo_Completo_Model::$contrato,
            Trabajo_Completo_Model::$nContratista,
            Trabajo_Completo_Model::$estatal,
            Trabajo_Completo_Model::$federal,
            Trabajo_Completo_Model::$municipal,
            Trabajo_Completo_Model::$otros,
            Trabajo_Completo_Model::$concurso,
            Trabajo_Completo_Model::$ejerObra,
            Trabajo_Completo_Model::$gpoNivel)
            ->where(Trabajo_Completo_Model::$id ,$id )
            ->where(Trabajo_Completo_Model::$obra , $id)
            ->get()->toArray();

        return $arrayObras;

    }

    public function detalleFichaObra($id)
    {

        $arrayFicha = Trabajo_Completo_Model::select(
        DB::raw("CONCAT(latitud ,' ', longitud) as coordenadas"),
        Trabajo_Completo_Model::$descripcion,
        Trabajo_Completo_Model::$cct,
        Trabajo_Completo_Model::$tipo,
        Trabajo_Completo_Model::$nTipo,
        Trabajo_Completo_Model::$escuela,
        Trabajo_Completo_Model::$avance,
        Trabajo_Completo_Model::$domicilio,
        Trabajo_Completo_Model::$localidad,
        Trabajo_Completo_Model::$municipio,
        Trabajo_Completo_Model::$region,
        Trabajo_Completo_Model::$nivel,
        Trabajo_Completo_Model::$ejercicio,
        Trabajo_Completo_Model::$ejerConcurso,
        Trabajo_Completo_Model::$ejerFondo,
        Trabajo_Completo_Model::$latitud,
        Trabajo_Completo_Model::$longitud,
        Trabajo_Completo_Model::$fecInicio,
        Trabajo_Completo_Model::$fecTermino,
            Trabajo_Completo_Model::$fInicioReal,
            Trabajo_Completo_Model::$fFinReal,
        Trabajo_Completo_Model::$estatus,
        Trabajo_Completo_Model::$obra,
        Trabajo_Completo_Model::$nContratista,
        Trabajo_Completo_Model::$direccionContratista,
        Trabajo_Completo_Model::$telContratista,
        Trabajo_Completo_Model::$celContratista,
        Trabajo_Completo_Model::$alumnosAtendidos,
        Trabajo_Completo_Model::$nSupervisor,
        Trabajo_Completo_Model::$nJefe,
        Trabajo_Completo_Model::$nPrograma,
        Trabajo_Completo_Model::$cPrograma,
        Trabajo_Completo_Model::$nFondo,
        Trabajo_Completo_Model::$cFondo,
        Trabajo_Completo_Model::$ejercicio,
        Trabajo_Completo_Model::$programado,
        Trabajo_Completo_Model::$importeFallado,
        Trabajo_Completo_Model::$concurso,
        Trabajo_Completo_Model::$federal,
        Trabajo_Completo_Model::$estatal,
        Trabajo_Completo_Model::$municipal,
        Trabajo_Completo_Model::$otros,
        Trabajo_Completo_Model::$modalidadConc,
        Trabajo_Completo_Model::$tipoAdj,
        Trabajo_Completo_Model::$avanceFinanciero,
        Trabajo_Completo_Model::$representante,
            Trabajo_Completo_Model::$diasDiferimiento,
            Trabajo_Completo_Model::$diasProrroga,
            Trabajo_Completo_Model::$cincoMillar,
            Trabajo_Completo_Model::$dosMillar,
            Trabajo_Completo_Model::$amortizado,
            Trabajo_Completo_Model::$importeEstimado,
            Trabajo_Completo_Model::$anticipo,
            Trabajo_Completo_Model::$importeAnticipo,
            Trabajo_Completo_Model::$aperturaFecha,
            Trabajo_Completo_Model::$fecContratacion,
            Trabajo_Completo_Model::$inicio,
            Trabajo_Completo_Model::$termina,
            Trabajo_Completo_Model::$importeContrato,
            Trabajo_Completo_Model::$ejerObra,
            Trabajo_Completo_Model::$contrato,
            Trabajo_Completo_Model::$msgEstatus)
        ->where(Trabajo_Completo_Model::$id ,$id )
        ->where(Trabajo_Completo_Model::$obra , $id)
        ->get();

        $arrayAvance = Trabajo_Avance_Model::select(Trabajo_Avance_Model::$id,
            Trabajo_Avance_Model::$avance,
            Trabajo_Avance_Model::$fechaCaptura,
            Trabajo_Avance_Model::$comentarios)
        ->where(Trabajo_Avance_Model::$trabajo, "=" , $id )
        ->get();
        
        $arrayFotos = Trabajo_Avance_Model::select(Trabajo_Avance_Foto_Model::$tabla. self::PUNTO .Trabajo_Avance_Foto_Model::$ubicacion)
            ->join(Trabajo_Avance_Foto_Model::$tabla,
                Trabajo_Avance_Model::$tabla.self::PUNTO.Trabajo_Avance_Model::$id,
                "=",
                Trabajo_Avance_Foto_Model::$tabla.self::PUNTO.Trabajo_Avance_Foto_Model::$folioCaptura)
            ->where(Trabajo_Avance_Model::$trabajo, "=" , $id )
            ->get();


        //dd($arrayFicha);

        return view('detalle_ficha' , compact('arrayFicha' , 'arrayAvance' , 'arrayFotos'));

    }


}
