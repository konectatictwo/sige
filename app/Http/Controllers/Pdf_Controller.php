<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Mapa_Controller;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class Pdf_Controller extends Controller
{

    const PUNTO = ".";
    const EQUALS = "=";
    const PERMISO = "catalogo_plantilla";
    const INICIO_PARAMETRO = '$p{';
    const INICIO_ESPECIAL = '$e{';

    /**
     * Genera la ficha técnica de una Obra en PDF
     * @author Erik Villarreal
     * @return PDF view
     */
    public static function generarFichaPdf($idObra)
    {


        $arrayFicha = Mapa_Controller::obtenerFichaObra($idObra);
        
        $pdf = \PDF::loadView('ficha_obra_pdf',compact('arrayFicha'))
            ->setPaper('Letter')
            ->setOption('margin-top', '1mm')
            ->setOption('margin-bottom', '10mm')
            //->setOrientation('landscape')
            //->setOption('header-html',$htmlheader)->setOption('header-spacing',3)
            ->setOption('footer-left',utf8_decode('Fecha: ' . date("d-m-Y") ))
            ->setOption('footer-right',utf8_decode('Pagina: [page] de [topage]'))
            ->setOption('footer-font-size','10');

        return  $pdf->stream('Contrato.pdf');

    }

    /** Funci�n para imprimir el pagar� de Anticipo
     * @author Erik Villarreal
     * @return file #pdf
     */
    public static function imprimirReportePdf($ruta,$extension,$mime,$arrayParametros)
    {
        //Crear instancia de Jasper
        $jasper = new JasperPHP;

        $uuid = Helpers::generate_uuid();
        
        //Compilar el jrxml
        $jasper->compile(public_path() . $ruta . '.jrxml')->execute();

        //Otorgar permisos a archivo Jasper creado por la compilaci�n
        $fileJasper = public_path() . $ruta . '.jasper';
        
        //Generar un output para cambiar el nombre concatenando el uuid
        $output = public_path() . $ruta . "-" . $uuid;

        //Crear PDF a partir del archivo Jasper
        $jasper->process(
            public_path() . $ruta . '.jasper',
            $output,
            array($extension),
            $arrayParametros,
            array(
                  'driver' => 'mysql',
                  'username' => 'konectatic',
                  'password' => '9v123g9AAPS2oaqUIJ',
                  'host' => '93.188.166.144',
                  'database' => 'focus_dev',
                  'port' => '3306'
            )
        )->execute();
/*
        exec($jasper->output().' 2>&1', $output);
        print_r($output);
        die();*/

        //Otorgar permisos al archivo pdf creado a partir del archivo Jasper
        $file = public_path() . $ruta . "-" . $uuid . "." . $extension;
        chmod($file, 0777); 

        //Mostrar a Usuario final
        header("Content-Type: " . $mime);
        header("Content-disposition: inline; "); 
        readfile($file);

        //Eliminar archivo pdf creado para dar paso a nuevos
        @unlink(public_path() . $ruta . ".jasper");
        @unlink(public_path() . $ruta . "-" . $uuid . ".pdf");
        

    }

}
