<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Mapa_Controller;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Trabajo_Completo_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class Excel_Controller extends Controller
{

    const PUNTO = ".";
    const EQUALS = "=";
    const PERMISO = "catalogo_plantilla";
    const INICIO_PARAMETRO = '$p{';
    const INICIO_ESPECIAL = '$e{';

    /**
     * Genera la ficha técnica de una Obra en PDF
     * @author Erik Villarreal
     * @return PDF view
     */
    public static function generarTablaExcel()
    {

        $data = request()->all();

        $arrayId = explode(",", $data["tabla"][0]);
        //dd($arrayId);

        $listadoObras = Trabajo_Completo_Model::find($arrayId);

        //dd($listadoObras);

        //return view('tabla_excel', compact("listadoObras"));
        \Excel::create('Listado de Obras', function($objPHPExcel) use($listadoObras){
            
            $objPHPExcel->sheet('Componente', function($sheet) use($listadoObras){
                $sheet->loadView('tabla_excel', ['listadoObras' => $listadoObras]);
            });

        })->download('xlsx');

    }

}
