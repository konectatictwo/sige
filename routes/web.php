<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');

});

Route::get('/','Mapa_Controller@index');

Route::post('/obtenerObras','Mapa_Controller@obtenerObras');

Route::get('detalleFichaObra/{id}','Mapa_Controller@detalleFichaObra');
Route::get('/generarFichaPdf/{id}','Pdf_Controller@generarFichaPdf');
Route::post('/generarTablaExcel','Excel_Controller@generarTablaExcel');
Route::post('/obtenerObrasEjercicio','Mapa_Controller@obtenerObrasEjercicio');
Route::post('/buscarObras','Mapa_Controller@buscarObras');
