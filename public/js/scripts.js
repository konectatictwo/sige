// proteccion back button
window.onpageshow = function (evt) {
    // If persisted then it is in the page cache, force a reload of the page.
    if (evt.persisted) {
        document.body.style.display = "none";
        location.reload();
    }
};


//Array de prueba para las imágenes en el carrusel
var imagenesCarrusel = [
    [urlBase + '/images/15.jpg'],
    [urlBase + '/images/16.jpg'],
    [urlBase + '/images/14.jpg']
];

var locations;

$(document).ready(function () {

    $('.select2').select2();
    $('.mensajeTooltip').tooltip();

    obtenerObras();

    tablaObra = $('#obra').DataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        "columnDefs": [
            {
                "targets": [0, 1, 2, 3, 4],
                'className': 'text-center '
            }
        ],
        "columns": [
            {
                "orderable": false,
                "target": -1,
                "data": "i",
                "render": function (data, type, full, meta) {

                    return '<a href="#" data-toggle="modal" data-target="#modal-ficha_obra" class="btn btn-xs btn-success" data-id="' + data + '"  title="Ver"> <span class="glyphicon glyphicon-eye-open"></span></a>';

                }
            },
            {"data": "ejercicio"},
            {"data": "nombreTipo"},
            {"data": "nivel"},
            {"data": "escuela"},
            {"data": "region"},
            {"data": "municipio"},
            {"data": "localidad"},
            {"data": "modalidad"},
            {"data": "cct"},
            {"data": "titulo"},
            {"data": "nombreEstatus"},
            {"data": "supervisor"},
            {"data": "importeContrato"},
           // {"data": "monto"},
            {"data": "importeEjercido"},
            {"data": "estatus"},//convenioAdicional
            {"data": "nContratista"},
            {"data": "avance"},
            {"data": "avFinanciero"},
            {"data": "finicioReal"},
            {"data": "fFinReal"},
            {"data": "estatus"},//diferimiento
            {"data": "estatus"}//prorroga
        ],
        "fnRowCallback": function (nRow, data) {
            var clase;

            if (data["avance"] == 100) {
                clase = "success";
            }
            if (data["avance"] < 100 && data["avance"] > 0) {
                clase = "warning";
            }

            if (data["avance"] == 0) {
                clase = "danger";
            }

            $(nRow).addClass(clase);
            return nRow;
        }
    });

    /**
     * Método que realiza el Toggle del Offcanvas
     * @author KonectaTIC
     * @return void
     */
    $('[data-toggle=offcanvas]').click(function () {
        $('.row-offcanvas').toggleClass('active');
    });

    /**
     * Método que se ejecuta al abrirse el modal de la ficha de Obra
     * @author KonectaTIC
     * @return void
     */
    $('#modal-ficha_obra').on('show.bs.modal', function (e) {

        if (($(e.relatedTarget).data('id') != "" &&
            $(e.relatedTarget).data('id') != null &&
            $(e.relatedTarget).data('id') != undefined) || $(e.relatedTarget).data('id') == 0) {
            var i = $(e.relatedTarget).data('id');
        } else {
            var i = $("#idi").val();
        }
        var rutaAMapaEstatico = "https://www.google.com.mx/maps/?q="+
            markers[i].latitud +
            "," +
            markers[i].longitud;

        var rutaMapaEstatico = "https://maps.googleapis.com/maps/api/staticmap?center=" +
            markers[i].latitud +
            "," +
            markers[i].longitud +
            "&markers=color:green%7Clabel:Lugar%7C" +
            //"&markers=icon:"+
            //"http://imgur.com/vEy2N" + "|"+
            markers[i].latitud +
            "," +
            markers[i].longitud +
            "&zoom=10&size=200x200&path=weight:3%7Ccolor:blue%7Cenc:{coaHnetiVjM??_SkM??~R&key=AIzaSyAXd4duRTGn9kpCTRAFDs2IjCtxb3Isu9Q";

        $("#masDetalles").attr("href", urlBase + "/detalleFichaObra/" + markers[i].obra);
        $("#imprimirFicha").attr("href", urlBase + "/generarFichaPdf/" + markers[i].obra);
        $("#nombreObra").html(markers[i].titulo);
        $("#region_ficha_nivel_2").html(markers[i].region);
        $("#cct").html(markers[i].cct);
        $("#escuela2").html(markers[i].escuela);
        $("#ejercicio").html(markers[i].ejercicio);
        $("#modalidad").html(markers[i].tipoAdj);
        $("#alumnos2").html(markers[i].alumnosAtendidos + " Alumnos");

        var importeFallado = [];
        if(markers[i].importeFallado != null && markers[i].importeFallado != ".0000"){
            var auxiliarSplit = markers[i].importeFallado.split(".");
            var importeFalladoSplit = auxiliarSplit[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            importeFallado[0] = "$" + importeFalladoSplit + "." + auxiliarSplit[1][0]  + auxiliarSplit[1][1];
        }else{
            importeFallado[0] = "$0.00";
        }

        //$("#monto_trabajo").html(importeFallado[0]);

        var importeContrato = [];
        if(markers[i].importeContrato != null && markers[i].importeContrato != ".0000"){
            var auxiliarSplit = markers[i].importeContrato.split(".");
            var importeFalladoSplit = auxiliarSplit[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            importeContrato[0] = "$" + importeFalladoSplit + "." + auxiliarSplit[1][0] + auxiliarSplit[1][1];
        }else{
            importeContrato[0] = "$0.00";
        }
        $("#monto_contratado").html(importeContrato[0]);

        var importeEjercido = [];
        if(markers[i].ejerObra != null && markers[i].ejerObra != ".0000"){
            var auxiliarSplit = markers[i].ejerObra.split(".");
            var importeEjercidoSplit = auxiliarSplit[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            importeEjercido[0] = "$" + importeEjercidoSplit + "." + auxiliarSplit[1][0] + auxiliarSplit[1][1];
        }else{
            importeEjercido[0] = "$0.00";
        }

        $("#monto_ejercido").html(importeEjercido[0]);
        $("#supervisor").html(markers[i].nSupervisor);
        $("#fecha_inicio").html(markers[i].finicioReal);
        $("#fecha_fin").html(markers[i].fFinReal);
        $("#duracionObra").html(markers[i].duracionDeObra+" días");
        $("#contrato").html(markers[i].contrato);
        $("#contratista").html(markers[i].nContratista);
        $("#financiamiento").html((markers[i].federal > 0 ? "Federal: " + markers[i].federal.split(".")[0] + "%" : "" ) + (markers[i].estatal > 0 ? " Estatal: " + markers[i].estatal.split(".")[0] + "%" : "") + (markers[i].municipal > 0 ? " Municipal: " + markers[i].municipal.split(".")[0] + "%" : "") + (markers[i].otros > 0 ? " Otros: " + markers[i].otros.split(".")[0] + "%" : ""));
        $("#icono").attr("src", markers[i].icon.url);

        $("#docConvocatoria").attr("href", "http://infejal.gob.mx/scanpdf?cnc=" + markers[i].concurso + "&td=60&nom=convocatoria.pdf");
        $("#docVisitaObra").attr("href", "http://infejal.gob.mx/scanpdf?cnc=" + markers[i].concurso + "&td=27&nom=convocatoria.pdf");
        $("#docApertura").attr("href", "http://infejal.gob.mx/scanpdf?cnc=" + markers[i].concurso + "&td=29&nom=convocatoria.pdf");
        $("#docFallo").attr("href", "http://infejal.gob.mx/scanpdf?cnc=" + markers[i].concurso + "&td=31&nom=convocatoria.pdf");
        $("#docContrato").attr("href", "http://infejal.gob.mx/scanpdf?cnc=" + markers[i].concurso + "&td=6&nom=convocatoria.pdf");
        $("#mapaEstatico").attr("src", rutaMapaEstatico);
        $("#mapaAEstatico").attr("href", rutaAMapaEstatico);

        if(markers[i].avance != null && markers[i].avance != undefined){
            var auxiliarAvance = markers[i].avance.split(".")[0];
        }else{
            var auxiliarAvance = "";
        }
        if (auxiliarAvance == "" || auxiliarAvance == null || auxiliarAvance == undefined) {
            auxiliarAvance = 0;
        }
        
        $("#spanPorcentajeAvance").html(auxiliarAvance);
        $("#barraPorcentajeAvance").css("width", auxiliarAvance + "%");
        
        if(markers[i].avFinanciero != null && markers[i].avFinanciero != undefined){
            var auxiliarAvanceFinanciero = markers[i].avFinanciero.split(".")[0];
            var aux = markers[i].avFinanciero.split(".");
            var avance = aux[0] + "." + aux[1][0] + aux[1][1];
        }else{
            var auxiliarAvanceFinanciero = "";
            var aux = "";
            var avance = "";
        }
        if (auxiliarAvanceFinanciero == "" || auxiliarAvanceFinanciero == null || auxiliarAvanceFinanciero == undefined) {
            auxiliarAvanceFinanciero = 0;
        }
        if (avance == "" || avance == null || avance == undefined) {
            avance = 0;
        }

        $("#spanPorcentajeAvanceFinanciero").html(avance);
        $("#barraPorcentajeAvanceFinanciero").css("width", auxiliarAvanceFinanciero + "%");
        //llenarCarrusel(imagenesCarrusel);
    });

    /**
     * Método que se ejecuta al cambiar de tab
     * @author KonectaTIC
     * @return void
     */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        google.maps.event.trigger(map, 'resize');
    });

});

/**
 * Método que sirve para llenar el carrusel del modal de la fiicha de Obra con las imágenes correspondientes a la Obra
 * @author KonectaTIC
 * @return void
 */
function llenarCarrusel(imagenes) {
    $('.carousel-inner').html("");
    $('.carousel-indicators').html("");

    for (var i = 0; i < imagenes.length; i++) {
        $('<div class="item"><img src="' + imagenes[i] + '"><div class="carousel-caption"></div>   </div>').appendTo('.carousel-inner');
        $('<li data-target="#carrusel-modal" data-slide-to="' + i + '"></li>').appendTo('.carousel-indicators')
    }

    $('#carrusel-modal').addClass('carousel slide');
    $('.item').first().addClass('active');
    $('.carousel-indicators > li').first().addClass('active');
    $('#carrusel-modal').carousel();
}

var geocoder;
var map;
var markers = [];
var bounds = new google.maps.LatLngBounds();
var estadoJalisco;

/**
 * Método que sirve para inicializar el mapa iniciando en un punto por default si no hay georeferencias
 * @author KonectaTIC
 * @return void
 */
function initialize() {
    map = new google.maps.Map(
        document.getElementById("map_canvas"), {
            center: new google.maps.LatLng(20.6593561, -103.3801584),
            zoom: 20,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
    );

    var imageBounds = {
        north: 22.7490,//mayor
        south: 18.9273,//menor
        east: -101.5122,//mayor
        //west: -105.7114//menor
        west: -105.760//menor
    };

    estadoJalisco = new google.maps.GroundOverlay(
        urlBase + "/images/jalisco.png",
        imageBounds);
    estadoJalisco.setMap(map);

    //Por el momento no hacemos instancia de Geo porque sólo acomodamos marcas por coordenadas dadas
    //geocoder = new google.maps.Geocoder();

    var contador;
    var i = 0;
    $.each(locations, function (index) {

        markers.push(prepararMarcas(locations[index], index));
        agregarRowTabla(index);
        //geocodeAddress(locations[index], index);
        i++;
    });
    filtrar();
    tablaObra.draw();
    

}

function prueba(){
    //console.log(tablaObra.data());
    var cadenaId = [];
    $.each(tablaObra.data(), function (index) {

        cadenaId[index] = tablaObra.data()[index]["idObra"];
        //console.log();

    });
    console.log(tablaObra.data());
    $("#tabla").val(cadenaId);
    $("#form").submit();
}

    /**
     * Método que sirve para agregar los Row al DataTable
     * @author KonectaTIC
     * @params index #Identificador puesto a cada registro al iniciar las marcas
     * @return void
     */
    function agregarRowTabla(index){
            var avanceFinanciero = [];
            var avanceFisico = [];
            var importeFallado = [];
            var importeContrato = [];
            var importeEjercido = [];

            if(locations[index]["AvFinanciero"] != null && locations[index]["AvFinanciero"] != ".0000"){
                avanceFinanciero = locations[index]["AvFinanciero"].toString().split(".");
                avanceFinanciero[0] += "%";
            }else{
                avanceFinanciero[0] = "0%";
            }

            if(locations[index]["Avance"] != null && locations[index]["Avance"] != ".0000"){
                avanceFisico = locations[index]["Avance"].toString().split(".");
                avanceFisico[0] += "%";
            }else{
                avanceFisico[0] = "0%";
            }

            if(locations[index]["ImporteFallado"] != null  && locations[index]["ImporteFallado"] != undefined && locations[index]["ImporteFallado"] != ".0000"){
                var auxiliarSplit = locations[index]["ImporteFallado"].toString().split(".");
                var importeFalladoSplit = auxiliarSplit[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                importeFallado[0] = "$" + importeFalladoSplit + "." + auxiliarSplit[1][0] + auxiliarSplit[1][1];
            }else{
                importeFallado[0] = "$0.00";
            }

            if(locations[index]["ImporteContrato"] != null  && locations[index]["ImporteContrato"] != undefined && locations[index]["ImporteContrato"] != ".0000"){
                var auxiliarSplit = locations[index]["ImporteContrato"].toString().split(".");
                var importeContratoSplit = auxiliarSplit[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                importeContrato[0] = "$" + importeContratoSplit + "." + auxiliarSplit[1][0] + auxiliarSplit[1][1];
            }else{
                importeContrato[0] = "$0.00";
            }

            if(locations[index]["EjerObra"] != null && locations[index]["EjerObra"] != undefined && locations[index]["EjerObra"] != ".0000"){
                var auxiliarSplit = locations[index]["EjerObra"].toString().split(".");
                var importeEjercidoSplit = auxiliarSplit[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                importeEjercido[0] = "$" + importeEjercidoSplit + "." + auxiliarSplit[1][0] + auxiliarSplit[1][1];
            }else{
                importeEjercido[0] = "$0.00";
            }

            tablaObra.row.add({
                "titulo": locations[index]["Descripcion"],
                "cct": locations[index]["Cct"],
                "escuela": locations[index]["Escuela"],
                "nivel": locations[index]["Nivel"],
                "estatus": locations[index]["Estatus"],
                "avance": avanceFisico[0],
                "region": locations[index]["Region"],
                "domicilio": locations[index]["Domicilio"],
                "localidad": locations[index]["Localidad"],
                "municipio": locations[index]["Municipio"],
                "ejercicio": locations[index]["Ejercicio"],
                "trabajo": locations[index]["Trabajo"],
                "modalidad": locations[index]["TipoAdj"],
                "supervisor": locations[index]["NSupervisor"],
                //"monto": importeFallado[0],
                "importeContrato": importeContrato[0],
                "importeEjercido": importeEjercido[0],
                "nContratista": locations[index]["NContratista"],
                "tipo": locations[index]["Tipo"],
                "nombreTipo": locations[index]["NTipo"],
                "nombreEstatus": locations[index]["MsgEstatus"],
                "avFinanciero": avanceFinanciero[0],
                "finicioReal": locations[index]["FInicioReal"],
                "fFinReal": locations[index]["FFinReal"],
                "i": index,
                "idObra": locations[index]["Obra"]
            });

    }


/**
 * Método que sirve para hacer el filtrado por Ejercicio
 * @author KonectaTIC
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function filtroEjercicio(i) {
    if ($(dropDownEjercicio).val() != "Seleccionar ejercicio") {
        if (markers[i].ejercicio == $(dropDownEjercicio).val()) {
            sumarizacionFiltradoTipo(i);
        }
    } else {
        sumarizacionFiltradoTipo(i);
    }
}

/**
 * Método que sirve para hacer el filtrado por Tipo Construcción
 * @author KonectaTIC
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function filtroConstruccion(i) {
    if ($(checkBoxConstruccion).prop("checked") == true) {
        if (markers[i].tipo == 1 ||
            markers[i].tipo == 8 ||
            markers[i].tipo == 10) {
            filtroEscuelasAl100(i);
        }
    } else {
        filtroEscuelasAl100(i);
    }
}

/**
 * Método que sirve para hacer el filtrado por Tipo Sustitución de Aulas
 * @author KonectaTIC
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function filtroSustitucionAulas(i) {
    if ($(checkBoxSustitucionAulas).prop("checked") == true) {
        if (markers[i].tipo == 3 ||
            markers[i].tipo == 7 ||     
            markers[i].tipo == 15) {
            filtroEscuelasAl100(i);
        }
    } else {
        filtroEscuelasAl100(i);
    }
}

/**
 * Método que sirve para hacer el filtrado por Tipo Rehabilitación
 * @author KonectaTIC
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function filtroRehabilitacion(i) {
    if ($(checkBoxRehabilitacion).prop("checked") == true) {
        if (markers[i].tipo == 2 ||
            markers[i].tipo == 9 ||
            markers[i].tipo == 11 ||
            markers[i].tipo == 19 ||
            markers[i].tipo == 20 ||
            markers[i].tipo == 22 ||
            markers[i].tipo == 33) {
            filtroEscuelasAl100(i);
        }
    } else {
        filtroEscuelasAl100(i);
    }
}

/**
 * Método que sirve para hacer el filtrado por Tipo Sustitución de Aulas
 * @author KonectaTIC
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function filtroOtros(i) {
    if ($(checkBoxOtros).prop("checked") == true) {
        if (markers[i].tipo != 1 &&
            markers[i].tipo != 8 &&
            markers[i].tipo != 10 &&
            markers[i].tipo != 3 &&
            markers[i].tipo != 7 &&
            markers[i].tipo != 15 &&
            markers[i].tipo != 2 &&
            markers[i].tipo != 9 &&
            markers[i].tipo != 11 &&
            markers[i].tipo != 19 &&
            markers[i].tipo != 20 &&
            markers[i].tipo != 22 &&
            markers[i].tipo != 33) {
            filtroEscuelasAl100(i);
        }
    } else {
        filtroEscuelasAl100(i);
    }
}

/**
 * Método que sirve para Sumarizar los filtros de Tipo
 * @author KonectaTIC
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function sumarizacionFiltradoTipo(i) {
    if ($(checkBoxConstruccion).prop("checked") == false &&
        $(checkBoxSustitucionAulas).prop("checked") == false &&
        $(checkBoxRehabilitacion).prop("checked") == false &&
        $(checkBoxOtros).prop("checked") == false) {
        filtroEscuelasAl100(i);
    } else {
        if ($(checkBoxConstruccion).prop("checked") == true) {
            filtroConstruccion(i);
        }
        if ($(checkBoxSustitucionAulas).prop("checked") == true) {

            filtroSustitucionAulas(i);
        }
        if ($(checkBoxRehabilitacion).prop("checked") == true) {
            filtroRehabilitacion(i);
        }
        if ($(checkBoxOtros).prop("checked") == true) {
            filtroOtros(i);
        }
    }
}

/**
 * Método que sirve para hacer el filtrado por Escuelas al 100
 * @author KonectaTIC
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function filtroEscuelasAl100(i) {
    if ($(checkBoxEscuelasAl100).prop("checked") == true) {
        if (markers[i].escuelasAlCien == 1) {
            filtroNivel(i);
        }
    } else {
        filtroNivel(i);
    }
}

/**
 * Método que sirve para hacer el filtrado por Nivel Educativo
 * @author KonectaTIC
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function filtroNivel(i) {
    if ($(dropDownNivel).val() != "Seleccionar nivel") {
        if (markers[i].nivel == $(dropDownNivel).val()) {
            filtroMunicipio(i);
        }
    } else {
        filtroMunicipio(i);
    }
}

/**
 * Método que sirve para hacer el filtrado por Municipio
 * @author KonectaTIC
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function filtroMunicipio(i) {
    if ($(dropDownMunicipio).val() != null) {
        if ($(dropDownMunicipio).val().length > 1) {
            var arrayAuxiliar = $(dropDownMunicipio).val();
            for (var j = 0; j < $(dropDownMunicipio).val().length; j++) {
                if (markers[i].municipio == arrayAuxiliar[j]) {
                    filtroEstatus(i);
                }
            }
        } else {
            if ($(dropDownMunicipio).val()[0] != "Seleccionar municipio") {
                if (markers[i].municipio == $(dropDownMunicipio).val()[0]) {
                    filtroEstatus(i);
                }
            } else {
                filtroEstatus(i);
            }
        }
    } else {
        filtroEstatus(i);
    }

}

/**
 * Método que sirve para hacer el filtrado por Priorizado
 * @author KonectaTIC
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function filtroPrioridad(i) {
    if ($(checkBoxPrioridad).prop("checked") == true) {
        if (markers[i].prioridad == 1) {
            filtroEstatus(i);
        }
    } else {
        filtroEstatus(i);
    }
}

/**
 * Método que sirve para hacer el filtrado por Estatus
 * @author KonectaTIC
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function filtroEstatus(i) {
    if ($(checkBoxTerminadas).prop("checked") == false &&
        $(checkBoxEnProceso).prop("checked") == false
    //$(checkBoxFiniquitadas).prop("checked") == false &&
    //$(checkBoxPrefiniquitadas).prop("checked") == false
    ) {
        markers[i].setMap(map);
        agregarRowTabla(i);

    } else {
        //Terminadas ó Finiquitadas
        if ($(checkBoxTerminadas).prop("checked") == true) {
            if (markers[i].estatus == "5" || markers[i].estatus == "2") {
                markers[i].setMap(map);
                agregarRowTabla(i);
            }
        }
        //En proceso ó Prefiniquitadas
        if ($(checkBoxEnProceso).prop("checked") == true) {
            if (markers[i].estatus == "0" || markers[i].estatus == "3") {
                markers[i].setMap(map);
                agregarRowTabla(i);
            }
        }
    }
}

var dropDownEjercicio = "#dropdownEjercicio";
var dropDownNivel = "#dropdownNivel";
var dropDownMunicipio = "#dropdownMunicipio";

var checkBoxConstruccion = "#checkboxConstruccion";
var checkBoxSustitucionAulas = "#checkboxSustitucionAulas";
var checkBoxRehabilitacion = "#checkboxRehabilitacion";
var checkBoxOtros = "#checkboxOtros";
var checkBoxPrioridad = "#checkboxPrioridad";
var checkBoxEscuelasAl100 = "#checkboxEscuelasAl100";

var checkBoxTerminadas = "#checkboxTerminadas";
var checkBoxEnProceso = "#checkboxEnProceso";
var checkBoxFiniquitadas = "#checkboxFiniquitadas";
var checkBoxPrefiniquitadas = "#checkboxPrefiniquitadas";

/**
 * Método que sirve para borrar todas las marcas y la tabla para iniciar el filtrado
 * @author KonectaTIC
 * @return void
 */
function filtrar() {
    tablaObra.clear();

    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }

    for (var i = 0; i < markers.length; i++) {
        filtroEjercicio(i); //Inicio de filtrado Cascada
    }

    tablaObra.draw();

}

/**
 * Listener que sirve para redimencionar el mapa al detectar cambio de tamaño de la ventana
 * @author KonectaTIC
 * @return void
 */
google.maps.event.addDomListener(window, "resize", function () {
    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
});

/**
 * Método  usado como intermediaro el cual prepara la info  que se pondrá en cada marca
 * @author KonectaTIC
 * @params locations #Array de la información obtenida de las obras
 * @params i #Identificador que se pondrá a las marcas
 * @return void
 */
function prepararMarcas(locations, i) {
    var string = locations["coordenadas"];
    var resultado = string.split(" ");
    var coordenadas = new google.maps.LatLng(parseFloat(resultado[0]), parseFloat(resultado[1]));
    return createMarker(coordenadas, locations, i);
}

/**
 * Método No usado por el momento, encargado de buscar direcciones o coordenadas con ayuda de la BD de Google
 * @author KonectaTIC
 * @params locations #Array de la información obtenida de las obras
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function geocodeAddress(locations, i) {
    var title = locations["Descripcion"];
    var address = locations["coordenadas"];
    var url = locations["Escuela"];

    setTimeout(function () {
        geocoder.geocode({

                'address': locations["coordenadas"]
            },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var marker = new google.maps.Marker({
                        icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                        map: map,
                        position: results[0].geometry.location,
                        title: title,
                        animation: google.maps.Animation.DROP,
                        address: address,
                        url: url
                    })

                    // infoWindow(marker, map, title, address, url);
                    bounds.extend(marker.getPosition());
                    map.fitBounds(bounds);
                } else {
                    alert("geocode of " + address + " failed:" + status);
                }
            });
    }, 2500);


}

//Variable que mantiene la ventana de la información que se muestra en la marca que está activa
var currentMark;

/**
 * Método encargado de crear los mensajes desplegables al pasar por una marca con el mouse
 * @author KonectaTIC
 * @params marker #Marca a la que se le pondrá la ventana desplegable
 * @return void
 */
function infoWindow(marker) {

    google.maps.event.addListener(marker, 'mouseover', function () {
        var auxiliarAvance = marker.avance.split(".")[0];
        if (auxiliarAvance == "" || auxiliarAvance == null || auxiliarAvance == undefined) {
            auxiliarAvance = 0;
        }

        var auxiliarAvanceFinanciero = 0;
        var avance = 0;
        if(marker.avFinanciero != null){
        auxiliarAvanceFinanciero = marker.avFinanciero.split(".")[0];
            var aux = marker.avFinanciero.split(".");
            avance = aux[0] + "." + aux[1][0] + aux[1][1];
        if (auxiliarAvanceFinanciero == "" || auxiliarAvanceFinanciero == null || auxiliarAvanceFinanciero == undefined) {
            auxiliarAvanceFinanciero = 0;
            avance = 0;
        }
        }

        var html = "<div style='font-size: 9px; padding-right: 0;'>" +
            "<div style='text-align: center; font-size: 12px; '>" +
            "<b>" +
            marker.nombreTipo +
            "</b>" +
            "</div>" +
            "<table style='margin-top: 0!important; padding-top: 0!important;'>" +
            "<tr>" +
            "<td>" +
            "<img src='" + marker.icon.url + "' style='margin-right: 15px; margin-top: 0; height: 60px; width: auto;'>" +
            "</td>" +
            "<td>" +
            "<table>" +
            '<tr style="padding-left: 15px;text-align: left!important;">' +
            '<td><b>Nivel Educativo:&nbsp;&nbsp;</b></td>' +
            '<td id="escuela">' + marker.nivel + '</td>' +
            '</tr>' +
            '<br>' +
            '<tr style="padding-left: 15px;text-align: left!important;">' +
            '<td><b>Escuela:&nbsp;&nbsp;</b></td>' +
            '<td id="nivel">' + marker.escuela + '</td>' +
            '</tr>' +
            '<br>' +
            '<tr style="padding-left: 15px;text-align: left!important;">' +
            '<td><b>Municipio:&nbsp;&nbsp;</b></td>' +
            '<td id="programa">' + marker.municipio + '</td>' +
            '</tr>' +
            '<br>' +
            '<tr style="padding-left: 15px;text-align: left!important;">' +
            '<td><b>Dirección:&nbsp;&nbsp;</b></td>' +
            '<td id="alumnos">' + marker.domicilio + '</td>' +
            '</tr>' +
            '<br>' +
            '<tr style="padding-left: 15px;text-align: left!important;">' +
            '<td><b>Descripción:&nbsp;&nbsp;</b></td>' +
            '<td id="domicilio">' + marker.titulo + '</td>' +
            '</tr>' +
            "</table>" +
            "</td>" +
            "</tr>" +
            "</table>" +
            "<hr style='margin: 0em;border-width: 2px;'>" +
            "<h6><span>" + auxiliarAvance + "% de Avance Físico</span></h6>" +
            '<div class="progress" style="height: 10px;">' +

            '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:' + marker.avance + '%">' +
            '</div>' +
            '</div>' +

            "<h6><span>" + avance + "% de Avance Financiero</span></h6>" +
            '<div class="" style="height: 10px;">' +

            '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:' + auxiliarAvanceFinanciero + '%">' +
            '</div>' +
            '</div>' +

            "<input id='idObra' type='hidden' value=" + marker.obra + "></input>" +
            "<input id='idi' type='hidden' value=" + marker.i + "></input>" +
            "</div>";

        iw = new google.maps.InfoWindow({
            content: html,
            maxWidth: 220,
            maxHeight: 200
        });
        iw.open(map, marker);
        currentMark = iw;
    });
    google.maps.event.addListener(marker, 'mouseout', function () {
        currentMark.close();
    });
    google.maps.event.addListener(marker, 'click', function () {
        $('#modal-ficha_obra').modal('show');
    });
}

/**
 * Método encargado de redimencionar el ícono a poner en las marcas
 * @author KonectaTIC
 * @params ruta #Ruta del ícono a redimencionar
 * @return icon
 */
function redimencionarIcono(ruta) {
    return icon = {
        url: ruta, // url
        scaledSize: new google.maps.Size(35, 50), // scaled size
        origin: new google.maps.Point(0, 0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };
}

/**
 * Método encargado de crear las marcas sobre el mapa filtrando tipo y estatus para colocar el ícono correcto
 * @author KonectaTIC
 * @params position #Objeto google maps que contiene las coordenadas
 * @params locations #Array que contiene la información de las obras
 * @params i #Identificador puesto a cada registro al iniciar las marcas
 * @return void
 */
function createMarker(position, locations, i) {
    var stringTipo = "Tipo";
    var stringEstatus = "Estatus";
    if (locations[stringTipo] == 1 ||
        locations[stringTipo] == 8 ||
        locations[stringTipo] == 10) {

        if (locations[stringEstatus] == "5") {
            icono = redimencionarIcono(urlBase + "/images/construccion-proceso.png");
        } else if (locations[stringEstatus] == "0" || locations[stringEstatus] == "3") {
            icono = redimencionarIcono(urlBase + "/images/construccion-finiquitada.png");
        } else if (locations[stringEstatus] == "2" ) {
            icono = redimencionarIcono(urlBase + "/images/construccion-proceso.png");
        } else if (locations[stringEstatus] == "4") {
            icono = redimencionarIcono(urlBase + "/images/construccion-cancelada.png");
        } else {
            icono = 'http://maps.google.com/mapfiles/ms/icons/orange.png';
        }


    } else if (locations[stringTipo] == 3 ||
        locations[stringTipo] == 7 ||
        locations[stringTipo] == 15) {

        if (locations[stringEstatus] == "5") {
            icono = redimencionarIcono(urlBase + "/images/sustitucion-aulas-proceso.png");
        } else if (locations[stringEstatus] == "0" || locations[stringEstatus] == "3") {
            icono = redimencionarIcono(urlBase + "/images/sustitucion-aulas-finiquitada.png");
        } else if (locations[stringEstatus] == "2" ) {
            icono = redimencionarIcono(urlBase + "/images/sustitucion-aulas-proceso.png");
        } else if (locations[stringEstatus] == "4") {
            icono = redimencionarIcono(urlBase + "/images/sustitucion-aulas-cancelada.png");
        } else {
            icono = 'http://maps.google.com/mapfiles/ms/icons/orange.png';
        }

    } else if (locations[stringTipo] == 2 ||
        locations[stringTipo] == 9 ||
        locations[stringTipo] == 11 ||
        locations[stringTipo] == 19 ||
        locations[stringTipo] == 20 ||
        locations[stringTipo] == 22 ||
        locations[stringTipo] == 33) {

        if (locations[stringEstatus] == "5") {
            icono = redimencionarIcono(urlBase + "/images/rehabilitacion-proceso.png");
        } else if (locations[stringEstatus] == "0" || locations[stringEstatus] == "3") {
            icono = redimencionarIcono(urlBase + "/images/rehabilitacion-finiquitada.png");
        } else if (locations[stringEstatus] == "2" ) {
            icono = redimencionarIcono(urlBase + "/images/rehabilitacion-proceso.png");
        } else if (locations[stringEstatus] == "4") {
            icono = redimencionarIcono(urlBase + "/images/rehabilitacion-cancelada.png");
        } else {
            icono = 'http://maps.google.com/mapfiles/ms/icons/orange.png';
        }

    } else {
        if (locations[stringEstatus] == "5") {
            icono = redimencionarIcono(urlBase + "/images/otros-terminada.png");
        } else if (locations[stringEstatus] == "0" || locations[stringEstatus] == "3") {
            icono = redimencionarIcono(urlBase + "/images/otros-proceso.png");
        } else if (locations[stringEstatus] == "2" ) {
            icono = redimencionarIcono(urlBase + "/images/otros-terminada.png");
        } else if (locations[stringEstatus] == "4") {
            icono = redimencionarIcono(urlBase + "/images/otros-proceso.png");
        } else {
            icono = 'http://maps.google.com/mapfiles/ms/icons/orange.png';
        }
    }

    var marker = new google.maps.Marker({
        icon: icono,
        map: map,
        position: position,
        animation: google.maps.Animation.DROP,
        latitud: locations["latitud"],
        longitud: locations["longitud"],
        titulo: locations["Descripcion"],
        cct: locations["Cct"],
        escuela: locations["Escuela"],
        nivel: locations["GpoNivel"],
        avance: locations["Avance"],
        avanceFinanciero: locations["AvFinanciero"],
        region: locations["Region"],
        domicilio: locations["Domicilio"],
        localidad: locations["Localidad"],
        municipio: locations["Municipio"],
        ejercicio: locations["Ejercicio"],
        tipo: locations["Tipo"],
        obra: locations["Obra"],
        fechaInicio: locations["FecInicio"],
        fechaTermino: locations["FecTermino"],
        estatus: locations["Estatus"],
        prioridad: locations["Prioridad"],
        alumnosAtendidos: locations["AlumnosAtendidos"],
        cPrograma: locations["CPrograma"],
        modalidadConc: locations["ModalidadConc"],
        importeFallado: locations["ImporteFallado"],
        nSupervisor: locations["NSupervisor"],
        nombreTipo: locations["NTipo"],
        avFinanciero: locations["AvFinanciero"],
        tipoAdj: locations["TipoAdj"],
        escuelasAlCien: locations["EscuelasAlCien"],
        finicioReal: locations["FInicioReal"],
        fFinReal: locations["FFinReal"],
        duracionDeObra: locations["DuracionDeObra"],
        importeContrato: locations["ImporteContrato"],
        contrato: locations["Contrato"],
        nContratista: locations["NContratista"],
        federal: locations["Federal"],
        estatal: locations["Estatal"],
        municipal: locations["Municipal"],
        otros: locations["Otros"],
        concurso: locations["Concurso"],
        ejerObra: locations["EjerObra"],
        i: i
    })
    bounds.extend(marker.getPosition());
    map.fitBounds(bounds);
    infoWindow(marker);
    return marker;
}


/**
 * Método que sirve para hacer el filtrado por coincidencia con la caja de texto
 * Usado sólo en la caja de búsqueda por el momento
 * @author KonectaTIC
 * @return void
 */
function busquedaLocal() {
    tablaObra.clear();

    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }


    var valorBusqueda = $("#cajaBusqueda").val();
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].cct.search(valorBusqueda) != -1 ||
            markers[i].escuela.search(valorBusqueda) != -1 ||
            markers[i].nivel.search(valorBusqueda) != -1 ||
            markers[i].region.search(valorBusqueda) != -1 ||
            markers[i].domicilio.search(valorBusqueda) != -1 ||
            markers[i].localidad.search(valorBusqueda) != -1 ||
            markers[i].municipio.search(valorBusqueda) != -1 ||
            markers[i].ejercicio.search(valorBusqueda) != -1 ||
            markers[i].obra.search(valorBusqueda) != -1 ||
            markers[i].titulo.search(valorBusqueda) != -1) {
            filtrarTipoB(i);
        }
    }

    tablaObra.draw();
}

/**
 * Método que sirve para hacer el filtrado de las marcas y mandar llamar la función de agregar row a la tabla
 * Usado sólo en la caja de búsqueda por el momento
 * @author KonectaTIC
 * @return void
 */
function filtrarTipoB(i) {

    if ($(checkBoxConstruccion).prop("checked") == false &&
        $(checkBoxSustitucionAulas).prop("checked") == false &&
        $(checkBoxRehabilitacion).prop("checked") == false) {

        filtroEstatus(i);

    } else {

        if ($(checkBoxConstruccion).prop("checked") == true) {
            if (markers[i].tipo == 1 ||
                markers[i].tipo == 8 ||
                markers[i].tipo == 10) {
                filtroEstatus(i);
            }
        }

        if ($(checkBoxSustitucionAulas).prop("checked") == true) {
            if (markers[i].tipo == 3 ||
                markers[i].tipo == 7 ||
                markers[i].tipo == 15) {
                filtroEstatus(i);
            }
        }

        if ($(checkBoxRehabilitacion).prop("checked") == true) {
            if (markers[i].tipo == 2 ||
                markers[i].tipo == 9 ||
                markers[i].tipo == 11 ||
                markers[i].tipo == 19 ||
                markers[i].tipo == 20 ||
                markers[i].tipo == 22 ||
                markers[i].tipo == 33) {
                filtroEstatus(i);
            }
        }

    }

}

/**
 * Método que sirve para hacer la consulta de las obras al entrar a la página
 * @author KonectaTIC
 * @return void
 */
function obtenerObras() {

    $.ajax({
        url: 'obtenerObras',
        data: {
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            locations = message;
            initialize();
        }, error: function (message) {

            alert("error");
        }
    });

}
  
