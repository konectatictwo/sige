/**
 * Created by Oscar Vargas on 26/12/2016.
 */


var geocoder;
var map;
var markers = [];
var bounds = new google.maps.LatLngBounds();

var imagenesCarrusel = [
    [urlBase + '/images/15.jpg'],
    [urlBase + '/images/16.jpg'],
    [urlBase + '/images/14.jpg']
];

$(document).ready(function() {

    llenarCarrusel(imagenesCarrusel);

});

/**
 * Método que sirve para llenar el carrusel del modal de la fiicha de Obra con las imágenes correspondientes a la Obra
 * @author KonectaTIC
 * @return void
 */
function llenarCarrusel(imagenes) {
    $('.carousel-inner').html("");
    $('.carousel-indicators').html("");

    for (var i = 0; i < imagenes.length; i++) {
        $('<div class="item"><img src="' + imagenes[i] + '"><div class="carousel-caption"></div>   </div>').appendTo('.carousel-inner');
        $('<li data-target="#carrusel-modal" data-slide-to="' + i + '"></li>').appendTo('.carousel-indicators')
    }

    $('#carrusel-modal').addClass('carousel slide');
    $('.item').first().addClass('active');
    $('.carousel-indicators > li').first().addClass('active');
    $('#carrusel-modal').carousel();
}

/** Método no utilizado debido a que el mapa es inicializado al momento de obtener toda la info de las Obras
 * Listener que sirve para inicializar la configuración al cargar la vista
 * @author KonectaTIC
 * @return void
 **/
 google.maps.event.addDomListener(window, "load", initialize);

/**
 * Método que sirve para inicializar el mapa iniciando en un punto por default si no hay georeferencias
 * @author Erik Villarreal
 * @return void
 */
function initialize() {
    map = new google.maps.Map(
        document.getElementById("map_canvas_ficha"), {
            center: new google.maps.LatLng(20.6593561, -103.3801584),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
    );

        markers.push(prepararMarcas(coordenadasBD));

}

/**
 * Método  usado como intermediaro el cual prepara la info  que se pondrá en cada marca
 * @author Erik Villarreal
 * @return void
 */
function prepararMarcas(locations) {
    var string = locations;
    var resultado = string.split(" ");
    var coordenadas = new google.maps.LatLng(parseFloat(resultado[0]), parseFloat(resultado[1]));
    return createMarker(coordenadas);
}

/**
 * Método encargado de crear las marcas sobre el mapa
 * @author Erik Villarreal
 * @return void
 */
function createMarker(position) {

        icono = 'http://maps.google.com/mapfiles/ms/icons/blue.png';

    var marker = new google.maps.Marker({
        icon: icono,
        map: map,
        position: position,
        animation: google.maps.Animation.DROP,
    })
    bounds.extend(marker.getPosition());
    map.fitBounds(bounds);
    map.setZoom(30);
    return marker;
}