<?php
use App\Models\Trabajo_Completo_Model;
?>

@extends('layouts.listado')
@section('title')
    SIGE - Detalle
@endsection
@section('links')

    <style type="text/css">
        .noPadding td{
            padding: 0!important;
        }
        .noPaddingFechas td{
            padding: 6px!important;
        }
        .alineamientoTextoDerecha{
            text-align: right;
        }
        .alineamientoTextoIzquierda{
            text-align: left;
        }
    </style>

@endsection
@section('scripts')
    <script>

        var coordenadasBD = '{{ $arrayFicha[0]->{ "coordenadas" } }}';
        var avance = '{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$avance} }}';

    </script>
    {!! Html::script('js/detalle.js') !!}
@endsection
@section('content')


<div class="page-header">
    <h1>Ficha de la Obra
        <small></small>
    </h1>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="btn-group pull-right">
                </div>
                <h3 class="panel-title">Datos Generales</h3>
            </div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <table class="table noPadding">
                        <tr>
                            <td><b>Nivel:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$nivel } }}</td>
                        </tr>
                        <tr>
                            <td><b>Escuela:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$escuela } }}</td>
                        </tr>
                        <tr>
                            <td><b>Región:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$region} }}</td>
                        </tr>
                        <tr>
                            <td><b>Municipio:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$municipio} }}</td>
                        </tr>
                        <tr>
                            <td><b>Localidad:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$localidad} }}</td>
                        </tr>
                        <tr>
                            <td><b>Beneficiados:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$alumnosAtendidos } . " Alumnos" }}</td>
                        </tr>
                        <tr>
                            <td><b>Programa:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$nPrograma} }}</td>
                        </tr>
                    </table>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <table class="table noPadding">
                        <tr>
                            <td><b>Contrato:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$contrato} }}</td>
                        </tr>
                        <tr>
                            <td><b>CCT:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$cct} }}</td>
                        </tr>
                        <tr>
                            <td><b>Normatividad:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$tipoAdj } }}</td>
                        </tr>
                        <tr>
                            <td><b>Modalidad:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$modalidadConc} }}</td>
                        </tr>
                        <tr>
                            <td><b>Concurso:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$concurso} }}</td>
                        </tr>
                        <tr>
                            <td><b>Fondo:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$nFondo } ."      Clave:" . $arrayFicha[0]->{ Trabajo_Completo_Model::$cFondo } }}</td>
                        </tr>
                        <tr>
                            <td><b>Estatus General:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$msgEstatus } }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="table noPadding">
                        <tr>
                            <td><b>Descripción:</b></td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$descripcion } }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="btn-group pull-right">
                </div>
                <h3 class="panel-title">Financiero</h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table>
                        <tr>
                            <td><b>Federal:</b></td>
                            <td>
                                {{ number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$federal}, 0 , "." , ",") . " %" }}
                            </td>


                            <td><b>Estatal:</b></td>
                            <td>
                                {{ number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$estatal}, 0 , "." , ",") . " %" }}
                            </td>


                            <td><b>Municipal:</b></td>
                            <td>
                                {{ number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$municipal}, 0 , "." , ",") . " %" }}
                            </td>


                            <td><b>Otros:</b></td>
                            <td>
                                {{ number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$otros}, 0 , "." , ",") . " %" }}
                            </td>
                    </tr>
                </table>
                </div>
                <br>
                <br>
                <br>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <h4>Montos</h4>
                        <table class="table noPadding">
                            <tr>
                                <td style="width: 150px;"><b>Monto Contratado:</b></td>
                                <td>{{ "$ ".number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$importeContrato} , 2 , "." , ",") }}</td>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><b>Monto Ejercido:</b></td>
                                <td>{{ "$ ".number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$ejerObra} , 2 , "." , ",") }}</td>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><b>Saldo por Ejercer:</b></td>
                                <?php $saldoEjercido = $arrayFicha[0]->{ Trabajo_Completo_Model::$importeContrato} - $arrayFicha[0]->{ Trabajo_Completo_Model::$ejerObra}; ?>
                                <td>{{ "$ ".number_format((string)$saldoEjercido , 2 , "." , ",") }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <h4>Convenios</h4>
                        <table class="table noPadding">
                            <tr>
                                <td style="width: 200px;"><b>Convenio Adicional:</b></td>
                                <td>No definido</td>
                            </tr>
                            <tr>
                                <td style="width: 200px;"><b>Anticipo de Convenio:</b></td>
                                <td>No definido</td>
                            </tr>
                            <tr>
                                <td style="width: 200px;"><b>Monto Total:</b></td>
                                <td>No definido</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <h4>Anticipo</h4>
                        <table class="table noPadding">
                            <tr>
                                <td><b>Porcentaje:</b></td>
                                <td>
                                    @if($arrayFicha[0]->{ Trabajo_Completo_Model::$anticipo} != null)
                                        {{ explode('.',$arrayFicha[0]->{ Trabajo_Completo_Model::$anticipo})[0] }}%
                                    @else
                                        0%
                                    @endif

                                </td>
                            </tr>
                            <tr>
                                <td><b>Monto:</b></td>
                                <td>{{ "$ ".number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$importeAnticipo} , 2 , "." , ",") }}</td>
                            </tr>
                            <tr>
                                <td><b>Monto Cobrado:</b></td>
                                <td>{{ "$ ".number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$amortizado} , 2 , "." , ",") }}</td>
                            </tr>
                            <tr>
                                <td><b>Saldo por Amortizar:</b></td>
                                <td>
                                    <?php $saldoAmortizar = $arrayFicha[0]->{ Trabajo_Completo_Model::$importeAnticipo} - $arrayFicha[0]->{ Trabajo_Completo_Model::$amortizado}; ?>
                                    {{ "$ ".number_format((string)$saldoAmortizar , 2 , "." , ",") }}
                                <td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <h4>Retenciones</h4>
                        <table class="table noPadding">
                            <tr>
                                <td><b>5 al Millar</b></td>
                                <td>{{ "$ ".number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$cincoMillar} , 2 , "." , ",") }}</td>
                            </tr>
                            <tr>
                                <td><b>2 al Millar</b></td>
                                <td>{{ "$ ".number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$dosMillar} , 2 , "." , ",") }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                    </div>
                    <h3 class="panel-title">Responsable INFEJAL</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>Jefe de Obra:</td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$nJefe } }}</td>
                        </tr>
                        <tr>
                            <td>Supervisor:</td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$nSupervisor } }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                    </div>
                    <h3 class="panel-title">Información Contratista</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>Contratista:</td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$nContratista } }}</td>
                        </tr>
                        <!--<tr>
                            <td>Direccion:</td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$direccionContratista } }}</td>
                        </tr>
                        <tr>
                            <td>Telefonos:</td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$telContratista } }}</td>
                        </tr>
                        <tr>
                            <td>Celular:</td>
                            <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$celContratista } }}</td>
                        </tr>-->
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                    </div>
                    <h3 class="panel-title">Ubicación</h3>
                </div>
                <div class="panel-body">
                    <div id="map_canvas_ficha" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="btn-group pull-right">
                </div>
                <h3 class="panel-title">Fechas</h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <table class="table noPaddingFechas">
                            <tr>
                                <td><b>Fecha de Apertura:</b></td>
                                <td>
                                    @if($arrayFicha[0]->{ Trabajo_Completo_Model::$aperturaFecha } != null)
                                        {{ $arrayFicha[0]->{ Trabajo_Completo_Model::$aperturaFecha } }}</td>
                                    @else
                                        No definida
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Fecha de Adjudicación:</b></td>
                                <td>
                                    @if($arrayFicha[0]->{ Trabajo_Completo_Model::$fecContratacion } != null)
                                        {{ $arrayFicha[0]->{ Trabajo_Completo_Model::$fecContratacion } }}
                                    @else
                                        No definida
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <table class="table noPaddingFechas">
                            <tr>
                                <td><b>Inicio de Contrato:</b></td>
                                <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$inicio } }}</td>
                            </tr>
                            <tr>
                                <td><b>Término de Contrato:</b></td>
                                <td>{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$termina } }}</td>
                            </tr>
                            <!--<tr>
                                <td><b>Fecha de Inicio Recalendarizada:</b></td>
                                <td></td>
                            </tr>-->
                        </table>
                    </div>
                    <!--<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <table class="table noPaddingFechas">
                            <tr>
                                <td><b>Fecha de Termino Recalendarizada:</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Recalendarizaciones Autorizadas:</b></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>-->
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <table class="table noPaddingFechas">
                            <tr>
                                <td><b>Días de Diferimiento:</b></td>
                                <td>
                                    @if($arrayFicha[0]->{ Trabajo_Completo_Model::$diasDiferimiento } != null)
                                        {{ $arrayFicha[0]->{ Trabajo_Completo_Model::$diasDiferimiento } }}
                                    @else
                                        0
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Días de Prórroga:</b></td>
                                <td>
                                    @if($arrayFicha[0]->{ Trabajo_Completo_Model::$diasProrroga } != null)
                                        {{ $arrayFicha[0]->{ Trabajo_Completo_Model::$diasProrroga } }}
                                    @else
                                        0
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                    </div>
                    <h3 class="panel-title">Reportes de Avance Físico</h3>
                </div>
                <div class="panel-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <table class="table table-striped table-bordered table-hover small" id="t_avance_fisico">
                                <thead>
                                    <tr>
                                        <!--<th>Foto</th>-->
                                        <th>Reporte</th>
                                        <th>Porcentaje</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($arrayAvance as $avance)
                                    <tr>
                                        <!--<td class="text-center">
                                                <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                                                data-toggle="modal" data-target="#modal-foto"
                                                data-id="{{ $avance->{ 'FolioCaptura' } }}"><span
                                                    class="glyphicon glyphicon-eye-open"></span></a>
                                        </td>-->

                                        <td>{{ $avance->{ "Comentarios" } }}</td>
                                        <td>
                                            @if($avance->{ "Avance" } != null)
                                                {{ explode('.',$avance->{ "Avance" })[0] }}%
                                            @else
                                                0%
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                    </div>
                    <h3 class="panel-title">Expediente Digital</h3>
                </div>
                <div class="panel-body">
                    
                    <p><b>Documentos</b></p>
                    <table>
                        <tr>
                            <td>
                                <img src="{{url('images/PDF.png')}}" style="height: 30px; width: auto;">
                                <a href="http://infejal.gob.mx/scanpdf?cnc={{ $arrayFicha[0]->{ Trabajo_Completo_Model::$concurso} }}&td=60&nom=convocatoria" id="docConvocatoria" target="_blank">
                                    <b>Convocatoria</b>
                                </a>
                            </td>
                            <td>
                                <img src="{{url('images/PDF.png')}}" style="height: 30px; width: auto;">
                                <a href="http://infejal.gob.mx/scanpdf?cnc={{ $arrayFicha[0]->{ Trabajo_Completo_Model::$concurso} }}&td=27&nom=visita-obra" id="docVisitaObra" target="_blank">
                                    <b>Visita de Obra</b>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="{{url('images/PDF.png')}}" style="height: 30px; width: auto;">
                                <a href="http://infejal.gob.mx/scanpdf?cnc={{ $arrayFicha[0]->{ Trabajo_Completo_Model::$concurso} }}&td=29&nom=documento-apertura" id="docApertura" target="_blank">
                                    <b>Apertura</b>
                                </a>
                            </td>
                            <td>
                                <img src="{{url('images/PDF.png')}}" style="height: 30px; width: auto;">
                                <a href="http://infejal.gob.mx/scanpdf?cnc={{ $arrayFicha[0]->{ Trabajo_Completo_Model::$concurso} }}&td=31&nom=fallo" id="docFallo" target="_blank">
                                    <b>Fallo</b>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="{{url('images/PDF.png')}}" style="height: 30px; width: auto;">
                                <a href="http://infejal.gob.mx/scanpdf?cnc={{ $arrayFicha[0]->{ Trabajo_Completo_Model::$concurso} }}&td=6&nom=contrato" id="docContrato" target="_blank">
                                    <b>Contrato</b>
                                </a>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                    </div>
                    <h3 class="panel-title">Avance de Obra</h3>
                </div>
                <div class="panel-body">

                    <div style="text-align: center;">
                        <h6>
                            <span>{{ number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$avance}, 0 , "." , ",")."%" }} de Avance Físico</span>
                        </h6>
                        <div class="progress" style="height: 5px;">

                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$avance} }}"
                             aria-valuemin="0" aria-valuemax="100" style="width:{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$avance}."%" }}">
                            </div>

                        </div>
                    </div>

                    <div style="text-align: center;">
                        <h6>
                            <span>{{ number_format($arrayFicha[0]->{ Trabajo_Completo_Model::$avanceFinanciero}, 0 , "." , ",")."%" }} de Avance Financiero</span>
                        </h6>
                        <div class="progress" style="height: 5px;">

                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$avanceFinanciero} }}"
                             aria-valuemin="0" aria-valuemax="100" style="width:{{ $arrayFicha[0]->{ Trabajo_Completo_Model::$avanceFinanciero}."%" }}">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                <div id="carrusel-modal" class="ajustarTamanios" data-ride="carousel">
                    <ol class="carousel-indicators"></ol>
                    <div class="carousel-inner"></div>
                    <a class="left carousel-control" href="#carrusel-modal" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carrusel-modal" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>

    </div>

</div>
@endsection
