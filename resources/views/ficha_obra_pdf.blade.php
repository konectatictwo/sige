
<?php
    use App\Models\Catalogos\Tipo_Obra_Model;
    use App\Models\Trabajo_Completo_Model;
?>

@extends('layouts.layout_pdf')
@section('title')
    SIGE
@endsection
@section('links')
@endsection
@section('scripts')
@endsection
@section('content')

            <div class="modal-header">
                    <img style="width: 200px; height: auto; padding-top: 10px;" src="{{url('images/educacion.png')}}">

                    <img src="{{url('images/INFEJAL-03.png ')}}" style="padding-left: 10px; width: 200px; height: auto; padding-top: 10px;">
                    <div style="margin-left: 600px; position: fixed; top: 100px;">
                        <h1>Ficha Técnica</h1>
                    </div>
            </div>
            <div style="text-align: center;">
                <h4 id="nombreObra" style="">{{ $arrayFicha[0]["Descripcion"] }}</h4>
                <h6>
                    <span><span id="spanPorcentajeAvance">{{ number_format($arrayFicha[0]["Avance"], 0 , "." , ",")."%" }}</span> de Avance Físico</span>
                </h6>
                <div class="progress" style="height: 10px; margin-right: 20px; margin-left: 20px;">
                        
                    <div id="barraPorcentajeAvance" title=""
                    class="progress-bar progress-bar-success mensajeTooltip" role="progressbar" aria-valuenow="{{ $arrayFicha[0]['Avance'] }}"
                        aria-valuemin="0" aria-valuemax="100" style="width:{{ $arrayFicha[0]['Avance'] }}%;">
                    </div>
                    
                </div>
                <h6>
                    <span><span id="spanPorcentajeAvanceFinanciero">{{ number_format($arrayFicha[0]["AvFinanciero"], 0 , "." , ",")."%" }}</span> de Avance Financiero</span>
                </h6>
                <div class="progress" style="height: 10px; margin-right: 20px; margin-left: 20px;">
                        
                    <div id="barraPorcentajeAvanceFinanciero" title=""
                    class="progress-bar progress-bar-success mensajeTooltip" role="progressbar" aria-valuenow="{{ $arrayFicha[0]['AvFinanciero'] }}"
                        aria-valuemin="0" aria-valuemax="100" style="width:{{ $arrayFicha[0]['AvFinanciero'] }}%;">
                    </div>
                    
                </div>
            </div>
            <div class="modal-body col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div>
                          <table style="top: 0!important; font-size: 16px;">
                                <tr >
                                    <td><b>Contrato:</b></td>
                                    <td id="contrato">{{ $arrayFicha[0]['Contrato'] }}</td>
                                </tr>
                                <tr >
                                    <td><b>CCT:</b></td>
                                    <td id="cct">{{ $arrayFicha[0]['Cct'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Escuela:</b></td>
                                    <td id="escuela2">{{ $arrayFicha[0]['Escuela'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Contratista:</b></td>
                                    <td id="contratista">{{ $arrayFicha[0]['NContratista'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Supervisor:</b></td>
                                    <td id="supervisor">{{ $arrayFicha[0]['NSupervisor'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Monto Contratado:</b></td>
                                    <td id="monto_contratado">{{ "$ ".number_format($arrayFicha[0]['ImporteContrato'] , 2 , "." , ",") }}</td>
                                </tr>
                                <tr>
                                    <td><b>Monto Trabajo:</b></td>
                                    <td id="monto_trabajo">{{ "$ ".number_format($arrayFicha[0]['ImporteFallado'] , 2 , "." , ",") }}</td>
                                </tr>
                                  <tr>
                                      <td><b>Monto Ejercido:</b></td>
                                      <td id="monto_ejercido">{{ "$ ".number_format($arrayFicha[0]['EjerObra'] , 2 , "." , ",") }}</td>
                                  </tr>




                                <tr>
                                    <td><b>Fuente de Financiamiento</b></td>
                                    <td id="financiamiento">
                                    {{ $arrayFicha[0]['AvFinanciero'] }}
                                    @if($arrayFicha[0]['Federal'] <> ".0000")
                                        {{ "Federal: " . number_format($arrayFicha[0]["Federal"], 0 , "." , ",") . " %" }}
                                        <br>
                                    @endif
                                    @if($arrayFicha[0]['Estatal'] <> ".0000")
                                        {{ "Estatal: " . number_format($arrayFicha[0]["Estatal"], 0 , "." , ",") . " %" }}
                                        <br>
                                    @endif
                                    @if($arrayFicha[0]['Municipal'] <> ".0000")
                                        {{ "Municipal: " . number_format($arrayFicha[0]["Municipal"], 0 , "." , ",") . " %" }}
                                        <br>
                                    @endif
                                    @if($arrayFicha[0]['Otros'] <> ".0000")
                                        {{ "Otros: " . number_format($arrayFicha[0]["Otros"], 0 , "." , ",") . " %" }}
                                    @endif
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>Beneficiados:</b></td>
                                    <td id="alumnos2">{{ $arrayFicha[0]['AlumnosAtendidos'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Modalidad:</b></td>
                                    <td id="modalidad">{{ $arrayFicha[0]['TipoAdj'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Fecha de Inicio:</b></td>
                                    <td id="fecha_inicio">{{ $arrayFicha[0]['FecInicio'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Fecha de Fin:</b></td>
                                    <td id="fecha_fin">{{ $arrayFicha[0]['FecTermino'] }}</td>
                                </tr>
                                <tr><!-- Días naturales entre Feche inicio a Fecha fin -->
                                    <td><b>Duración de Obra:</b></td>
                                    <td id="duracionObra">{{ $arrayFicha[0]['DuracionDeObra'] }}</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">

                                        <br>

                                        
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                    <div style="margin-top: 100px; text-align: center;">
                        <img style="margin-right: 10px;" width="300" height="300" src="{{url('/images/16.jpg')}}" >
                        <img style="margin-left: 50px; margin-right: 10px;" id="mapaEstatico" width="300" height="300" src="https://maps.googleapis.com/maps/api/staticmap?center={{ $arrayFicha[0]['latitud'] }},{{ $arrayFicha[0]['longitud'] }}&markers=color:green%7Clabel:Lugar%7C{{ $arrayFicha[0]['latitud'] }},{{ $arrayFicha[0]['longitud'] }}&zoom=10&size=200x200&path=weight:3%7Ccolor:blue%7Cenc:{coaHnetiVjM??_SkM??~R&key=AIzaSyAXd4duRTGn9kpCTRAFDs2IjCtxb3Isu9Q" >
                    </div>

                </div>

            </div>

             <div class="modal-footer" >
                
             </div>



@endsection


