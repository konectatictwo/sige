<?php
	use App\Models\Catalogos\Tipo_Obra_Model;
?>

@extends('layouts.layout')
@section('title')
    SIGE
@endsection
@section('links')
@endsection
@section('scripts')
    {!! Html::script('vendor/ms-Dropdown-master/js/msdropdown/jquery.dd.min.js') !!}
    {!! Html::script('js/scripts.js') !!}
@endsection
@section('content')

<style type="text/css">
  .tablaHorizontal{
    overflow: auto;
    /*max-height: 100%;*/
    height: calc(100% - 50px);
  }
</style>

<div class="tab-content" style="height: 100%;">
  <div id="home" class="tab-pane fade in active" style="height:100%;">

  	<div style="right: 0; padding-top: 30px; padding-right: 50px; position: fixed; z-index: 500;">
  		<img style="padding-left: 50px; height: 50px;" src="{{url('images/sige.png')}}">
  	</div>

    <div id="map_canvas" style="border: 2px solid #3872ac;">
      <div style="padding-top: 10%; padding-left: 35%; position: fixed; z-index: 500;">
        <img style="width: 400px; height: auto;" src="{{url('images/cargando.gif')}}"><br>
        <label style="text-align: center; width: 100%;">ACLARACIÓN:</label><br>
        <label> LA INFORMACIÓN PRESENTADA ESTA SUJETA A ACTUALIZACIONES CONSTANTES.</label>
      </div>
    </div>

  </div>
  <div id="busqueda" class="tab-pane fade" style="height:100%;">
      <br>

      <!--url("generarTablaExcel/-18598")"-->
      {!! Form::open(['id'=>'form', 'url' => url('generarTablaExcel'),'role' => 'form', 'class' => 'form', 'enctype'=>'multipart/form-data', 'file'=>true]) !!}
        <input type="hidden" name="tabla[]" id="tabla">
      {!! Form::close() !!}
    <div class="col-md-12 tablaHorizontal">
        <table class="table table-hover table-bordered small" id="obra" style="text-align: center;">
            <thead>
                <tr>
                    <th style="text-align: center;">Acciones</th>
                    <th style="text-align: center">Ejercicio</th>
                    <th style="text-align: center">Tipo de Acción</th>
                    <th style="text-align: center">Nivel Educativo</th>
                    <th style="text-align: center">Escuela</th>
                    <th style="text-align: center">Región</th>
                    <th style="text-align: center">Municipio</th>
                    <th style="text-align: center">Localidad</th>
                    <th style="text-align: center">Modalidad</th>
                    <th style="text-align: center">CCT</th>
                    <th style="text-align: center">Descripción</th>
                    <th style="text-align: center">Estatus</th>
                    <th style="text-align: center">Supervisor</th>
                    <th style="text-align: center">Monto Contratado</th>
                    <!--<th style="text-align: center">Monto Trabajo</th>-->
                    <th style="text-align: center">Monto Ejercido</th>
                    <th style="text-align: center">Convenio Adicional</th>
                    <th style="text-align: center">Contratista</th>
                    <th style="text-align: center">Av. Físico</th>
                    <th style="text-align: center">Av. Financiero</th>
                    <th style="text-align: center">Inicio Programado</th>
                    <th style="text-align: center">Término Programado</th>
                    <th style="text-align: center">Diferimiento</th>
                    <th style="text-align: center">Prórroga</th>
                </tr>
            </thead>
            <tbody style="text-align: center;">

            </tbody>
        </table>
        
    </div>
    <a id="" href="#" onclick="prueba();" class="btn btn-success btn-xs" style="right: 30px; bottom: 0; position: fixed; font-size: 14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
      <span>Descargar Excel</span>
    </a>
        
  </div>
  <div id="contacto" class="tab-pane fade in" style="height:100%;">

    <div class="container">
      
      <div style="text-align: center;">
            <img class="imagenesBot" style="padding-left: 5%; height: auto; width: 20%!important; padding-top: 3%;" src="{{url('images/educacion.png')}}">
            <img class="imagenesBot" src="{{url('images/INFEJAL-03.png ')}}" style="padding-left: 5%; height: auto; width: 20%!important; padding-top: 3%;">
      </div>

      <div class="col-md-offset-1 col-md-3 col-xs-12" style="margin-top: 20px;">
          <img src="{{url('images/personaje-infejal.png')}}" style="width: 100%; height: auto; top: 0;">
      </div>
      <div class="col-md-3 col-xs-12" style="margin-top: 20px;">
        <div class="input-group input-group-xs" style="width: 100%">
        <label style="margin-bottom: 10px;"><b>Nombre</b></label>
        <input type="text" value="" placeholder="" class="form-control " required>
      </div>
      <br>
      <div class="input-group input-group-sm" style="width: 100%" >
        <label style="margin-bottom: 10px;"><b>Correo electrónico</b></label>
        <input type="text" value="" placeholder="" class="form-control " required>
      </div>
        <a href="https://www.facebook.com/pg/infejal/posts/?ref=page_internal" target="_blank">
            <img src="{{url('images/facebook.png')}}" style="width: 40px; height: 40px; float: left;  position: fixed; margin-left: 200px; margin-top: 15px;">
        </a>
        <a href="https://twitter.com/infejal" target="_blank">
            <img src="{{url('images/twitter.png')}}" style="width: 40px; height: 40px; float: left; position: fixed; margin-left: 150px; margin-top: 15px;">
        </a>
        
    </div>

    <div class="col-md-4 col-xs-12" style="margin-top: 20px;">
      <div class="input-group input-group-xs" style="width: 100%; height: 100px;">
        <label style="margin-bottom: 10px;"><b>Comentario</b></label>
        <textarea type="text" value="" style=" height: 190px;" placeholder="" class="form-control " required></textarea>
      </div>
      <div class="input-group input-group-xs" style="width: 100%; height: 100px; margin-top: 10px;">
          <button class="btn btn-info" style="float: right; margin-top: 10px;">Enviar</button>
      </div>
    </div>

  </div>
    
  </div>
</div>    
       

@include("ficha_obra")


@endsection
