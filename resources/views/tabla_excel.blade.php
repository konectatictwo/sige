<?php
    use App\Models\Trabajo_Completo_Model;
?>
        <table class="table table-hover table-bordered small" id="obra" style="text-align: center;">
            <thead>
                <tr>
                    <th style="text-align: center">Obtenido en: </th>
                    <th style="text-align: center"><?php echo date("d-m-Y H:i:s"); ?></th>
                </tr>
            </thead>
            <tbody style="text-align: center;">
            </tbody>
        </table>
        <table class="table table-hover table-bordered small" id="obra" style="text-align: center;">
            <thead>
                <tr>
                    <th style="text-align: center">Ejercicio</th>
                    <th style="text-align: center">Tipo de Obra</th>
                    <th style="text-align: center">Nivel Educativo</th>
                    <th style="text-align: center">Escuela</th>
                    <th style="text-align: center">Región</th>
                    <th style="text-align: center">Municipio</th>
                    <th style="text-align: center">Localidad</th>
                    <th style="text-align: center">Modalidad</th>
                    <th style="text-align: center">CCT</th>
                    <th style="text-align: center">Descripción</th>
                    <th style="text-align: center">Estatus</th>
                    <th style="text-align: center">Supervisor</th>
                    <th style="text-align: center">Monto Contratado</th>
                    <th style="text-align: center">Monto Trabajo</th>
                    <th style="text-align: center">Monto Ejercido</th>
                    <th style="text-align: center">Convenio Adicional</th>
                    <th style="text-align: center">Contratista</th>
                    <th style="text-align: center">Av. Físico</th>
                    <th style="text-align: center">Av. Financiero</th>
                    <th style="text-align: center">Fecha Inicio</th>
                    <th style="text-align: center">Fecha Fin</th>
                    <th style="text-align: center">Diferimiento</th>
                    <th style="text-align: center">Prórroga</th>
                </tr>
            </thead>
            <tbody style="text-align: center;">
                @foreach($listadoObras as $row)
                <tr>
                    <td >{{$row->{Trabajo_Completo_Model::$ejercicio} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$nTipo} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$gpoNivel} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$escuela} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$region} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$municipio} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$localidad} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$tipoAdj} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$cct} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$descripcion} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$estatus} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$nSupervisor} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$importeContrato} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$importeFallado} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$ejerObra} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$estatus} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$nContratista} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$avance} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$avanceFinanciero} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$fecInicio} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$fecTermino} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$estatus} }}</td>
                    <td >{{$row->{Trabajo_Completo_Model::$estatus} }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    