<div class="modal fade" id="modal-alert-message-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-info">
                    <strong>Aviso!</strong>
                    <p id="info-message"></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#modal-alert-message-info').on('shown.bs.modal',function (e) {
            setTimeout(function(){
                $('#modal-alert-message-info').modal('hide');
            }, 3000);
        });
    });
</script>