<div class="modal fade" id="modal-alert-message-warning">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Aviso!</strong>
                    <p id="warning-message"></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#modal-alert-message-warning').on('shown.bs.modal',function (e) {
            setTimeout(function(){
                $('#modal-alert-message-warning').modal('hide');
            }, 3000);
        });
    });
</script>