<?php
use App\Http\Helpers\Helpers;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;

?>

<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
        {{--Opciones a la Izquierda--}}
        <ul class="nav navbar-nav small esconderVertical">

            <li>
                <img style="padding-left: 50px; height: 50px; padding-top: 10px;" src="{{url('images/educacion.png')}}">
            </li>
            <li>
                <img src="{{url('images/INFEJAL-03.png ')}}" style="padding-left: 50px; height: 50px; padding-top: 10px;">
            </li>

        </ul>

        {{--Opciones a la Derecha--}}
        <ul class="nav navbar-nav navbar-right small">

            <li>
                <img src="{{url('images/sigo.png ')}}" style="padding-left: 50px; height: 50px; padding-top: 10px;">
            </li>

        </ul>
    </div>
</nav>


