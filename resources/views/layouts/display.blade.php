<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ str_replace('_',' ',env('NOMBRE_SISTEMA')) }} - @yield('title')</title>
    <meta name="description" content="Sistema de Administracion en la Construccion">
    <meta name="author" content="Oscar Vargas & Erik Villarreal">
    {!! Html::style('vendor/bootstrap/css/bootstrap.css') !!}
    {!! Html::style('vendor/bootstrap/css/bootstrap-theme.min.css') !!}
    {!! Html::style('vendor/datatables/css/dataTables.bootstrap.min.css') !!}

    {!! Html::style('css/style.css') !!}
    @yield('links')
    {!! Html::script('vendor/bootstrap/js/jquery.min.js') !!}
    {!! Html::script('vendor/bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('vendor/datatables/js/jquery.dataTables.js') !!}
    {!! Html::script('vendor/datatables/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('js/scripts.js') !!}
    @yield('scripts')
    <style>
        table {
            border-collapse: collapse;
            width: 100%!important;
        }
        body {
            padding-top: 60px;
            padding-bottom: 60px;
            padding-left: 50px;
            padding-right: 50px;
        }
    </style>
</head>
<body>
<div class="container-fuild">

    @extends('layouts.menu_display')
    @if (Session::has('message'))
        <div class="modal fade" id="modal-alert-message">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="alert alert-success">
                            <strong>Aviso!</strong>
                            {{ Session::get('message') }}
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script type="text/javascript">
            $(document).ready(function () {
                $('#modal-alert-message').modal('show');
                $('#modal-alert-message').on('shown.bs.modal',function (e) {
                    setTimeout(function(){
                        $('#modal-alert-message').modal('hide');
                    }, 1500);
                });
            });
        </script>
    @endif

    @if (Session::has('error'))
        <div class="modal fade" id="modal-errors-message">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="alert alert-danger">
                            <strong>Error! </strong>
                            {{ Session::get('error') }}
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script type="text/javascript">
            $(document).ready(function () {
                $('#modal-errors-message').modal('show');
                $('#modal-errors-message').on('shown.bs.modal',function (e) {
                    setTimeout(function(){
                        $('#modal-errors-message').modal('hide');
                    }, 1500);
                });
            });
        </script>
    @endif
    
    <div class="row-offcanvas row-offcanvas-left">
        <div id="main">
        
            @yield('content')

        </div>
    </div>

    @extends('layouts.barra_inferior')
</div>
</body>
</html>

@include("Genericos.Alertas.error")
@include("Genericos.Alertas.info")
@include("Genericos.Alertas.success")
@include("Genericos.Alertas.warning")