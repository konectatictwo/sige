<?php
use App\Http\Helpers\Helpers;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;

?>
<style type="text/css">
    nav li a{
        padding: 0;
    }
    .bordeNegro{
        border: 1px solid black;
    }
    hr{
        margin: 0em;
        border-width: 2px;
    }
</style>
    <div class="menuLateral  esconder" >
            

        <div title="Desplazar menú" data-toggle="offcanvas" style=" height: 30px; 
                            width: 30px; 
                            margin-top: 25px;
                            background-image: url('{{url('images/menu.png ')}}');  
                            background-size: 100% 100%;
                            margin-right: 20px;
                            float: right;
                            cursor: pointer;
                            right: 10px;"
                            onclick="
                                $('.esconder').animate({width: 'toggle'}, 200,function(){
                                    google.maps.event.trigger(map, 'resize');
                                });
                                $('.esconderVertical').animate({height: 'toggle'});
                    ">
        </div>

        <div style="opacity: 1;">
            <img style="padding-top: 20px; padding-left: 20%; padding-bottom: 15px; height: auto; width: 75%;" src="{{url('images/sige.png')}}">
        </div>
        
        
        <!-- Menú de filtrado principal -->
        <ul class="nav nav-pills nav-stacked" style="top: 5px; margin-left: 10px; margin-bottom: 70px;">

            <li>
                <div style="width: 80%!important;">
                <div style="font-size: 13px; margin-left: 10px; color: black; margin-top: 5px;">Buscar:</div>
                <button style="float: right;  height: 30px; width: 13%; padding-left: 5px;" onclick="busquedaLocal();" class="glyphicon glyphicon-search"></button>
                <input id="cajaBusqueda" type="text" class="form-control bordeNegro" style="margin-left: 10px;  width: 80%; text-transform:uppercase; color: black; font-size: 13px;" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
            </li>

            <li>
                <div style="font-size: 13px; margin-left: 10px; color: black;">Municipios:</div>
                {!! Form::select( "municipio" , $municipios, 0, ['id' => "dropdownMunicipio" , 'class' => "form-control bordeNegro", "style" => "font-size: 13px; margin-left: 10px; margin-top: 2px; width: 80%!important; color: black; height: 100px;", "onchange" => "filtrar();" , "multiple" => true]) !!}
            </li>
            
            <li>
                <div style=" font-size: 13px; margin-left: 10px; color: black;">Ejercicio:</div>
                <select id="dropdownEjercicio" style="font-size: 13px; margin-left: 10px; margin-top: 2px; width: 80%!important; " onchange="filtrar();" 
                    data-toggle="tooltip" data-placement="left" class="form-control bordeNegro">
                    <option value="Seleccionar ejercicio">Todos</option>
                    <option value="2013">2013</option>
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016" selected="true">2016</option>
                    <option value="2017">2017</option>
                </select>
            </li>
            
            <li>
                <div style="font-size: 13px; margin-left: 10px; color: black;">Nivel Educativo:</div>
                {!! Form::select( "nivel" , $nivel, 0, ['id' => "dropdownNivel" , 'class' => "form-control bordeNegro", "style" => "font-size: 13px; margin-left: 10px; margin-top: 2px; width: 80%!important; color: black;", "onchange" => "filtrar();"]) !!}
            </li>

            <hr style="margin-top: 5px;">

            <!-- Estatus -->
            <div style="font-size: 13px; margin-left: 10px; color: black;">Estatus:</div>
            <li>
                <a href="#" onclick="$(this).find('input').click();" style="font-size: 13px; padding: 0;padding-top: 3px; margin-left: 10px">
                    <img src="{{url('images/finiquitada.png')}}" style="height: 25px; width: auto;">
                    <input id="checkboxEnProceso" type="checkbox" onclick="filtrar();" data-toggle="tooltip" data-placement="left">
                    En proceso
                </a>
            </li>
            <li style="margin-bottom: 10px;">
                <a href="#" onclick="$(this).find('input').click();" style="font-size: 13px; padding: 0; padding-top: 3px; margin-left: 10px">
                    <img src="{{url('images/proceso.png')}}" style="height: 25px; width: auto;">
                    <input id="checkboxTerminadas" type="checkbox" onclick="filtrar();" data-toggle="tooltip" data-placement="left">
                    Terminada
                </a>
            </li>
            <hr>

            <!--
            <li>
                <a style="font-size: 13px;" href="#" onclick="$(this).find('input').click();">
                    <img src="{{url('images/priorizada.png')}}" style="height: 30px; width: auto;">
                    <input id="checkboxPrioridad" type="checkbox" onclick="filtrar();" data-toggle="tooltip" style="font-size: 13px;" data-placement="left">
                    Priorizada
                </a>
            </li>-->

            <hr>

            <!-- Tipos -->

            <li style="padding-top: 5px; ">
                <a style="font-size: 13px;" href="#" data-toggle="collapse" data-target="#menuTipos" onclick=" $(this).find('span').toggleClass('glyphicon glyphicon-triangle-bottom').toggleClass('glyphicon glyphicon-triangle-right');">Tipo de Acción<span class="glyphicon glyphicon-triangle-right"></span></a>
            </li>

            <div id="menuTipos" class="collapse">
                <ul class="nav nav-pills nav-stacked" style="top: 5px; margin-left: 10px;">
                <li>
                    <a href="#" onclick="$(this).find('input').click();" style="font-size: 13px;">
                        <img src="{{url('images/construccion.png')}}" style="height: 30px; width: auto;">
                        <input id="checkboxConstruccion" type="checkbox" onclick="filtrar();" data-toggle="tooltip" data-placement="left">
                        Construcción
                    </a>
                </li>

                <li style="top: 0!important;">
                    <a href="#" onclick="$(this).find('input').click();" style="font-size: 13px;">
                        <img src="{{url('images/rehabilitacion.png')}}" style="height: 30px; width: auto;">
                        <input id="checkboxRehabilitacion" type="checkbox" onclick="filtrar();" data-toggle="tooltip" data-placement="left">
                        Rehabilitación
                    </a>
                </li>

                <li>
                    <a href="#" onclick="$(this).find('input').click();" style="font-size: 13px;">
                        <img src="{{url('images/equipamiento.png')}}" style="height: 30px; width: auto;">
                        <input id="checkboxSustitucionAulas" type="checkbox" onclick="filtrar();" data-toggle="tooltip" data-placement="left">
                        Equipamiento
                    </a>
                </li>

                <li>
                    <a href="#" onclick="$(this).find('input').click();" style="font-size: 13px;">
                        <img src="{{url('images/escuelas al cien.png')}}" style="height: 30px; width: auto;">
                        <input  id="checkboxEscuelasAl100" type="checkbox" onclick="filtrar();" data-toggle="tooltip" data-placement="left">
                        Escuelas al Cien
                    </a>
                </li>

                <li>
                    <a href="#" onclick="$(this).find('input').click();" style="font-size: 13px;">
                        <img src="{{url('images/otros-lateral.png')}}" style="height: 30px; width: auto;">
                        <input id="checkboxOtros" type="checkbox" onclick="filtrar();" data-toggle="tooltip" data-placement="left">
                        Otros
                    </a>
                </li>

                </ul>
            </div>
        </ul>
        
        <div id="imagenesBottom" style="position: fixed; bottom: 0; background-color: white; height: 10%; width: 15%; display: none; text-align: center;" class="esconder">
            <img class="imagenesBot" style="padding-left: 5%; height: auto; width: 30%; padding-top: 10%;" src="{{url('images/educacion.png')}}">
            <img class="imagenesBot" src="{{url('images/INFEJAL-03.png ')}}" style="padding-left: 5%; height: auto; width: 30%; padding-top: 10%;">
        </div>
    </div>

<div style="left: 0; width: 100%;top: 0;height: 15px;position: fixed; z-index: 2000;  background-image: url('images/franja_superior.png'); background-repeat: no-repeat; background-size: 100% 15px;">   
</div>

<nav style="background-color: white!important;" class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
        {{--Opciones a la Izquierda--}}
            <ul class="nav navbar-nav small esconderVertical">
                <li style=" height: 30px; 
                            width: 30px; 
                            top: 25px; 
                            background-image: url('{{url('images/menu.png ')}}');  
                            background-size: 100% 100%;
                            margin-left: 20px">
                    <a  title="Desplazar menú" data-toggle="offcanvas" style="cursor: pointer; color: black!important;" onclick="
                        $('.esconder').animate({width: 'toggle'}, 200,function(){
                            google.maps.event.trigger(map, 'resize');
                        });
                        $('.esconderVertical').animate({height: 'toggle'});
                    ">
                    </a>
                </li>
                <li>
                    <img style="padding-left: 50px; height: 70px; padding-top: 20px; padding-bottom: 10px;" src="{{url('images/educacion.png')}}">
                </li>
                <li>
                    <img src="{{url('images/INFEJAL-03.png ')}}" style="padding-left: 50px; height: 70px; padding-top: 20px; padding-bottom: 10px;">
                </li>

            </ul>

        {{--Opciones a la Derecha--}}
        <ul class="nav navbar-nav navbar-right small">

            <li class="active" ><a data-toggle="tab" href="#home" onclick="" style="height: 70px; padding-top: 30px; ">Mapa</a></li>
            <li><a data-toggle="tab" href="#busqueda" style="height: 70px; padding-top: 30px;">Tabla</a></li>
            <li><a data-toggle="tab" href="#contacto" style="height: 70px; padding-top: 30px;">Contáctanos</a></li>

        </ul>
    </div>
</nav>
<!--
<div style="left: 0; width: 100%;bottom: 0;height: 8px;position: fixed; z-index: 2000;  background-image: url('images/franja_superior.png'); background-repeat: no-repeat; background-size: 100% 8px;">   
</div>-->

