<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="description" content="Sistema de Administracion en la Construccion">
    <meta name="author" content="Oscar Vargas & Erik Villarreal">
    {!! Html::style('vendor/bootstrap/css/bootstrap.css') !!}
    {!! Html::style('vendor/bootstrap/css/bootstrap-theme.min.css') !!}
    {!! Html::style('vendor/datatables/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('css/style.css') !!}
    @yield('links')
    <script type="text/javascript">
        //Variable global que contiene la url base del servidor
        var urlBase = "{{url('/')}}";
        //Variable que contiene la configuración del lenguaje del Datatable
        var urlTable = "{{ asset('vendor/datatables/lang/es_MX.json') }}";
        var token = '{{ csrf_token() }}';
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXd4duRTGn9kpCTRAFDs2IjCtxb3Isu9Q"></script>
    {!! Html::script('vendor/bootstrap/js/jquery.min.js') !!}
    {!! Html::script('vendor/bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('vendor/datatables/js/jquery.dataTables.js') !!}
    {!! Html::script('vendor/datatables/js/dataTables.bootstrap.min.js') !!}
    @yield('scripts')
    <style>
        body {
            height: 100%;
            padding-top: 60px;
            padding-bottom: 60px;
            padding-left: 50px;
            padding-right: 50px;
        }
        nav{
            padding-right: 20px;
        }
        table {
            border-collapse: collapse;
            width: 100%!important;
        }
    </style>
    @yield('styles')
</head>
<body>
<div class="container-fuild" style="height: 100%;">
    @extends('layouts.menu_display')

    <div class="row-offcanvas row-offcanvas-left active">
        <div id="main">

            @yield('content')

        </div>
    </div>

</div>
</body>
</html>
