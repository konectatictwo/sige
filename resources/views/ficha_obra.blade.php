<style type="text/css">

</style>
<div class="modal fade" id="modal-ficha_obra">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                    <img style="height: 50px; padding-top: 10px;" src="{{url('images/educacion.png')}}">

                    <img src="{{url('images/INFEJAL-03.png ')}}" style="padding-left: 10px; height: 50px; padding-top: 10px;">
                    <div style="margin-left: 250px; position: fixed; top: 20px;">
                        <h3>Ficha Técnica</h3>
                    </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div style="text-align: center;">
                <h6 id="nombreObra" style=""></h6>
                <h6>
                    <span><span id="spanPorcentajeAvance"></span>% de Avance Físico</span>
                </h6>
                <div class="progress" style="height: 10px; margin-right: 20px; margin-left: 20px;">
                        
                    <div id="barraPorcentajeAvance" title=""
                    class="progress-bar progress-bar-success mensajeTooltip" role="progressbar" aria-valuenow="70"
                        aria-valuemin="0" aria-valuemax="100" style="width:70%;">
                    </div>
                    
                </div>
                <h6>
                    <span><span id="spanPorcentajeAvanceFinanciero">70</span>% de Avance Financiero</span>
                </h6>
                <div class="progress" style="height: 10px; margin-right: 20px; margin-left: 20px;">
                        
                    <div id="barraPorcentajeAvanceFinanciero" title=""
                    class="progress-bar progress-bar-success mensajeTooltip" role="progressbar" aria-valuenow="70"
                        aria-valuemin="0" aria-valuemax="100" style="width:70%;">
                    </div>
                    
                </div>
            </div>
            <div class="modal-body col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Carrusel aún no se usa en versión 1  
                <div id="carrusel-modal" class="ajustarTamanios" data-ride="carousel">
                     Indicators 
                    <ol class="carousel-indicators"></ol>
                     Wrapper for slides 
                    <div class="carousel-inner"></div>
                     Controls 
                    <a class="left carousel-control" href="#carrusel-modal" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carrusel-modal" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div> -->

                <div>
                          <table style="top: 0!important;">
                                <tr >
                                    <td><b>Contrato:</b></td>
                                    <td id="contrato"></td>
                                </tr>
                                <tr >
                                    <td><b>Región:</b></td>
                                    <td id="region_ficha_nivel_2"></td>
                                </tr>
                                <tr >
                                    <td><b>CCT:</b></td>
                                    <td id="cct"></td>
                                </tr>
                                <tr>
                                    <td><b>Escuela:</b></td>
                                    <td id="escuela2"></td>
                                </tr>
                                <tr>
                                    <td><b>Contratista:</b></td>
                                    <td id="contratista"></td>
                                </tr>
                                <tr>
                                    <td><b>Supervisor:</b></td>
                                    <td id="supervisor"></td>
                                </tr>
                                <tr>
                                    <td><b>Monto Contratado:</b></td>
                                    <td id="monto_contratado"></td>
                                </tr>
                                <!--<tr>
                                    <td><b>Monto Trabajo:</b></td>
                                    <td id="monto_trabajo"></td>
                                </tr>-->
                                  <tr>
                                      <td><b>Monto Ejercido:</b></td>
                                      <td id="monto_ejercido"></td>
                                  </tr>
                                <tr>
                                    <td><b>Fuente de Financiamiento</b></td>
                                    <td id="financiamiento"></td>
                                </tr>
                                <tr>
                                    <td><b>Beneficiados:</b></td>
                                    <td id="alumnos2"></td>
                                </tr>
                                <tr>
                                    <td><b>Modalidad:</b></td>
                                    <td id="modalidad"></td>
                                </tr>
                                <tr>
                                    <td><b>Inicio Programado:</b></td>
                                    <td id="fecha_inicio"></td>
                                </tr>
                                <tr>
                                    <td><b>Término Programado:</b></td>
                                    <td id="fecha_fin"></td>
                                </tr>
                                <tr><!-- Días naturales entre Feche inicio a Fecha fin -->
                                    <td><b>Duración de Obra:</b></td>
                                    <td id="duracionObra"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">

                                        <br>

                                        
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                    <div style=" text-align: center;">
                        <img style="margin-right: 10px;" width="200" height="200" src="{{url('/images/16.jpg')}}" >
                        <a id="mapaAEstatico" href="#" target="_blank">
                            <img style="margin-right: 10px; cursor: pointer;" id="mapaEstatico" width="200" height="200" src="" >
                        </a>
                    </div>

                    <hr>

                    <p><b>Documentos</b></p>
                    <table>
                        <tr>
                            <td>
                                <img src="{{url('images/PDF.png')}}" style="height: 30px; width: auto;">
                                <a href="" id="docConvocatoria" target="_blank">
                                    <b>Convocatoria</b>
                                </a>
                            </td>
                            <td>
                                <img src="{{url('images/PDF.png')}}" style="height: 30px; width: auto;">
                                <a href="" id="docVisitaObra" target="_blank">
                                    <b>Visita de Obra</b>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="{{url('images/PDF.png')}}" style="height: 30px; width: auto;">
                                <a href="" id="docApertura" target="_blank">
                                    <b>Apertura</b>
                                </a>
                            </td>
                            <td>
                                <img src="{{url('images/PDF.png')}}" style="height: 30px; width: auto;">
                                <a href="" id="docFallo" target="_blank">
                                    <b>Fallo</b>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="{{url('images/PDF.png')}}" style="height: 30px; width: auto;">
                                <a href="" id="docContrato" target="_blank">
                                    <b>Contrato</b>
                                </a>
                            </td>
                        </tr>
                    </table>

                </div>



            </div>

             <div class="modal-footer" >
                <div style="float: right;">
                    <a id="imprimirFicha" href="{{url("generarFichaPdf/-18598")}}" class="btn btn-success btn-xs" target="_blank" style="font-size: 14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                        <span>Imprimir ficha</span>
                    </a>
                    <a id="masDetalles" href="{{url("detalleFichaObra/-18598")}}" class="btn btn-success btn-xs" target="_blank" style="font-size: 14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                        <span>Más información</span>
                    </a>
                </div>

             </div>

        </div>
    </div>
</div>